//
//  WeatherForecastNetworkingTests.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import XCTest
import CoreLocation

@testable import WeatherForecast

let performanceTimeout : TimeInterval = 4

class WeatherForecastNetworkingTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func getCurrentWeatherDataForOneLocation(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        let coord = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(currentLocation)  in
                        
                        let openWeatherMap : OpenWeatherMap = OpenWeatherMap()
                        
                        openWeatherMap.getCurrentWeatherData(forOne: currentLocation,
                                                             onCompletion: {(currentWeather) in
                                                                
                                                                XCTAssertNotNil(currentWeather.date, "Date is not set correctly")
                                                                
                                                                XCTAssertNotNil(currentWeather.location, "Location is not set correctly")
                                                                XCTAssertNotNil(currentWeather.sunset, "Sunset is not set correctly")        // время заката
                                                                XCTAssertNotNil(currentWeather.sunrise, "Sunrise is not set correctly")      // время восхода
                                                                XCTAssertNotNil(currentWeather.temp, "Temp is not set correctly")            // текущая температура
                                                                XCTAssertNotNil(currentWeather.tempMin, "Temp_min is not set correctly")     // минимальная температура за сутки
                                                                XCTAssertNotNil(currentWeather.tempMax, "Temp_max is not set correctly")     // максимальная температура за сутки
                                                                XCTAssertNotNil(currentWeather.pressure, "Pressure is not set correctly")    // атмосферное давление
                                                                XCTAssertNotNil(currentWeather.humidity, "Humidity is not set correctly")    // влажность воздуха
                                                                XCTAssertNotNil(currentWeather.windSpeed, "WindSpeed is not set correctly")  // скорость ветра
                                                                XCTAssertNotNil(currentWeather.clouds, "Clouds is not set correctly")        // облачность
                                                                
                                                                XCTAssertNotNil(currentWeather.icon, "Icon is not set correctly")            // описание
                                                                XCTAssertNotNil(currentWeather.detail, "Detail is not set correctly")        // детальное описание
                                                                
                                                                
                                                                testExpectation.fulfill()
                        },
                                                             onError: {(code, message) in
                                                                XCTAssert(false, "errorCode=\(code); errorMessage=\(message)")
                                                                testExpectation.fulfill()
                        })
                        
                        
        },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
        })
        
        
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
    }
    
    func testYekaterinburgCurrentWeatherDataForOneLocation() {
        getCurrentWeatherDataForOneLocation(latitude: 56.817439, longitude: 60.631440)
    }
    
    func testGlubokoeCurrentWeatherDataForOneLocation() {
        getCurrentWeatherDataForOneLocation(latitude: 56.764894, longitude: 60.924968)
    }
    
    func testAtlanticOceanCurrentWeatherDataForOneLocation() {
        getCurrentWeatherDataForOneLocation(latitude: 54.336210, longitude: -19.748260)
    }

 
    func testAntarcticCurrentWeatherDataForOneLocation() {
        getCurrentWeatherDataForOneLocation(latitude: -80.614868, longitude: 52.178406)
    }
 
 
    
    func get5DayPer3HourForecastData(latitude: CLLocationDegrees, longitude: CLLocationDegrees) {
        
        let coord = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(currentLocation)  in
                        
                        let openWeatherMap : OpenWeatherMap = OpenWeatherMap()
                        
                        openWeatherMap.get5DayPer3HourForecastData(forOne: currentLocation,
                                                             onCompletion: {(weatherHours) in
                                                                
                                                                XCTAssert(weatherHours.count > 0, "Data not found")
                                                                for weatherHour in (weatherHours) {
                                                                    
                                                                    XCTAssertNotNil(weatherHour.location, "Location is not set correctly")
                                                                    XCTAssertNotNil(weatherHour.date, "Date is not set correctly")
                                                                    XCTAssertNotNil(weatherHour.temp, "Temp is not set correctly")             // текущая температура
                                                                    XCTAssertNotNil(weatherHour.tempMin, "Temp_min is not set correctly")      // минимальная температура за сутки
                                                                    XCTAssertNotNil(weatherHour.tempMax, "Temp_max is not set correctly")      // максимальная температура за сутки
                                                                    XCTAssertNotNil(weatherHour.pressure, "Pressure is not set correctly")     // атмосферное давление
                                                                    XCTAssertNotNil(weatherHour.humidity, "Humidity is not set correctly")     // влажность воздуха
                                                                    XCTAssertNotNil(weatherHour.windSpeed, "WindSpeed is not set correctly")   // скорость ветра
                                                                    XCTAssertNotNil(weatherHour.windDeg, "WindDeg is not set correctly")       // направление ветра
                                                                    XCTAssertNotNil(weatherHour.clouds, "Clouds is not set correctly")         // облачность
                                                                    XCTAssertNotNil(weatherHour.partsOfDay, "PartsOfDay is not set correctly") // Время суток
                                                                    XCTAssertNotNil(weatherHour.seaLevel, "Sea_level is not set correctly")    // Уровень моря
                                                                    XCTAssertNotNil(weatherHour.icon, "Icon is not set correctly")             // описание
                                                                    XCTAssertNotNil(weatherHour.detail, "Detail is not set correctly")         // детальное описание
                                                                    
                                                                }
                                                                
                                                                
                                                                testExpectation.fulfill()
                        },
                                                             onError: {(code, message) in
                                                                XCTAssert(false, "errorCode=\(code); errorMessage=\(message)")
                                                                testExpectation.fulfill()
                        })
                        
                        
        },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
        })
        
        
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
    }
    
    
    func testYekaterinburg5DayPer3HourForecastDataForOneLocation() {
        get5DayPer3HourForecastData(latitude: 56.817439, longitude: 60.631440)
    }
    
    func testGlubokoe5DayPer3HourForecastDataForOneLocation() {
        get5DayPer3HourForecastData(latitude: 56.764894, longitude: 60.924968)
    }
    
    func testAtlanticOcean5DayPer3HourForecastDataForOneLocation() {
        get5DayPer3HourForecastData(latitude: 54.336210, longitude: -19.748260)
    }
    
    
    func testAntarctic5DayPer3HourForecastDataForOneLocation() {
        get5DayPer3HourForecastData(latitude: -80.614868, longitude: 52.178406)
    }
    
}


