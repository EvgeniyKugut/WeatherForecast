//
//  WeatherForecastWeatherEngineTests.swift
//  WeatherForecast
//
//  Created by Evgeniy on 31.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import XCTest
import CoreLocation

@testable import WeatherForecast

class WeatherForecastWeatherEngineTests: XCTestCase {
    
    override func setUp() {
        super.setUp()
        // Put setup code here. This method is called before the invocation of each test method in the class.
    }
    
    override func tearDown() {
        // Put teardown code here. This method is called after the invocation of each test method in the class.
        super.tearDown()
    }
    
    func testWeatherEngineLimitedDateTimes() {
        let coord = CLLocationCoordinate2D(latitude: 56.817439, longitude: 60.631440)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(location)  in
                        
                        var weatherOnHours : Array<WeatherOnHour> = []
                        
                        let date1AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 15, minut: 00, second: 00)!
                        let date1AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 00, second: 00)!
                        let date1AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 17, minut: 00, second: 00)!
                        let date1AugAnd18Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 18, minut: 00, second: 00)!
                        
                        let date2AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 15, minut: 00, second: 00)!
                        let date2AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 16, minut: 00, second: 00)!
                        let date2AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 17, minut: 00, second: 00)!
                        
                        let date3AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 15, minut: 00, second: 00)!
                        let date3AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 16, minut: 00, second: 00)!
                        let date3AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 17, minut: 00, second: 00)!
                        
                        // add test data to random position
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd18Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd16Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd15Hours))
                        
                        let limit = WeatherEngine.getLimitedDateTimes(forWeatherOnHour: weatherOnHours)
                        XCTAssert(limit.0 == date1AugAnd15Hours, "min date (\(date1AugAnd15Hours)) != \(limit.0!)")
                        XCTAssert(limit.1 == date3AugAnd17Hours, "min date (\(date3AugAnd17Hours)) != \(limit.1!)")
                        
                        testExpectation.fulfill()
                        
        },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
        })
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
    }
    
    func testWeatherEngineTrimFrom() {
        let coord = CLLocationCoordinate2D(latitude: 56.817439, longitude: 60.631440)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(location)  in
                        
                        var weatherOnHours : Array<WeatherOnHour> = []
                        
                        let date1AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 15, minut: 00, second: 00)!
                        let date1AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 00, second: 00)!
                        let date1AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 17, minut: 00, second: 00)!
                        let date1AugAnd18Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 18, minut: 00, second: 00)!
                        
                        let date2AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 15, minut: 00, second: 00)!
                        let date2AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 16, minut: 00, second: 00)!
                        let date2AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 17, minut: 00, second: 00)!
                        
                        let date3AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 15, minut: 00, second: 00)!
                        let date3AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 16, minut: 00, second: 00)!
                        let date3AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 17, minut: 00, second: 00)!
                        
                        // add test data to random position
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd18Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd16Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd15Hours))
                        
                        let fromDate1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 30, second: 00)!
                        let weatherOnHourTimmed0 = WeatherEngine.trim(weatherOnHour: weatherOnHours, from: fromDate1)
                        XCTAssert(weatherOnHourTimmed0[0].date! == date1AugAnd16Hours, "dateFrom=\(fromDate1); weatherOnHourTimmed[0].date (\(weatherOnHourTimmed0[0].date!)) != \(date1AugAnd16Hours)")
                        
                        
                        let weatherOnHourTimmed1 = WeatherEngine.trim(weatherOnHour: weatherOnHours, from: date1AugAnd16Hours)
                        XCTAssert(weatherOnHourTimmed1[0].date! == date1AugAnd16Hours, "dateFrom=\(date1AugAnd16Hours); weatherOnHourTimmed[0].date (\(weatherOnHourTimmed1[0].date!)) != \(date1AugAnd16Hours)")
                        
                        testExpectation.fulfill()
                        
        },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
        })
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
    }
    
    func testWeatherEngineTrimTo() {
        let coord = CLLocationCoordinate2D(latitude: 56.817439, longitude: 60.631440)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(location)  in
                        
                        var weatherOnHours : Array<WeatherOnHour> = []
                        
                        let date1AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 15, minut: 00, second: 00)!
                        let date1AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 00, second: 00)!
                        let date1AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 17, minut: 00, second: 00)!
                        let date1AugAnd18Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 18, minut: 00, second: 00)!
                        
                        let date2AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 15, minut: 00, second: 00)!
                        let date2AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 16, minut: 00, second: 00)!
                        let date2AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 17, minut: 00, second: 00)!
                        
                        let date3AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 15, minut: 00, second: 00)!
                        let date3AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 16, minut: 00, second: 00)!
                        let date3AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 17, minut: 00, second: 00)!
                        
                        // add test data to random position
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd18Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd16Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd15Hours))
                        
                        let fromDate1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 30, second: 00)!
                        let weatherOnHourTimmed0 = WeatherEngine.trim(weatherOnHour: weatherOnHours, to: fromDate1)
                        XCTAssert(weatherOnHourTimmed0[weatherOnHourTimmed0.count - 1].date! == date1AugAnd17Hours, "dateTo=\(fromDate1); weatherOnHourTimmed[0].date (\(weatherOnHourTimmed0[weatherOnHourTimmed0.count - 1].date!)) != \(date1AugAnd17Hours)")
                        
                        
                        let weatherOnHourTimmed1 = WeatherEngine.trim(weatherOnHour: weatherOnHours, to: date1AugAnd17Hours)
                        XCTAssert(weatherOnHourTimmed1[weatherOnHourTimmed1.count - 1].date! == date1AugAnd17Hours, "dateTo=\(date1AugAnd17Hours); weatherOnHourTimmed[0].date (\(weatherOnHourTimmed1[weatherOnHourTimmed1.count - 1].date!)) != \(date1AugAnd17Hours)")
                        
                        testExpectation.fulfill()
                        
                     },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
                     })
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
    }
    
    func testWeatherEngineFindTemperatureDiff() {
        
        let coord = CLLocationCoordinate2D(latitude: 56.817439, longitude: 60.631440)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(location)  in
                        
                        var weatherOnHours1 : Array<WeatherOnHour> = []
                        var weatherOnHours2 : Array<WeatherOnHour> = []
                        
                        let date1AugAnd15Hours1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 15, minut: 00, second: 00)!
                        let date1AugAnd16Hours1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 00, second: 00)!
                        let date1AugAnd17Hours1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 17, minut: 00, second: 00)!
                        let date1AugAnd18Hours1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 18, minut: 00, second: 00)!
                        let date1AugAnd19Hours1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 19, minut: 00, second: 00)!
                        let date1AugAnd20Hours1 = Date.make(withYear: 2017, month: 08, day: 1, hour: 20, minut: 00, second: 00)!
                        

                        let date1AugAnd15Hours2 = Date.make(withYear: 2017, month: 08, day: 1, hour: 15, minut: 30, second: 00)!
                        let date1AugAnd16Hours2 = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 30, second: 00)!
                        let date1AugAnd17Hours2 = Date.make(withYear: 2017, month: 08, day: 1, hour: 17, minut: 30, second: 00)!
                        let date1AugAnd18Hours2 = Date.make(withYear: 2017, month: 08, day: 1, hour: 18, minut: 30, second: 00)!
                        let date1AugAnd19Hours2 = Date.make(withYear: 2017, month: 08, day: 1, hour: 19, minut: 30, second: 00)!
                        let date1AugAnd20Hours2 = Date.make(withYear: 2017, month: 08, day: 1, hour: 20, minut: 30, second: 00)!
                        
                        
                        let weatherOnHour1List1  = WeatherOnHour(for: location, at: date1AugAnd15Hours1)
                        weatherOnHour1List1.temp = 10
                        let weatherOnHour2List1  = WeatherOnHour(for: location, at: date1AugAnd16Hours1)
                        weatherOnHour2List1.temp = 11
                        let weatherOnHour3List1  = WeatherOnHour(for: location, at: date1AugAnd17Hours1)
                        weatherOnHour3List1.temp = 12
                        let weatherOnHour4List1  = WeatherOnHour(for: location, at: date1AugAnd18Hours1)
                        weatherOnHour4List1.temp = 13
                        let weatherOnHour5List1  = WeatherOnHour(for: location, at: date1AugAnd19Hours1)
                        weatherOnHour5List1.temp = 14
                        let weatherOnHour6List1  = WeatherOnHour(for: location, at: date1AugAnd20Hours1)
                        weatherOnHour6List1.temp = 15

                        
                        let weatherOnHour1List2  = WeatherOnHour(for: location, at: date1AugAnd15Hours2)
                        weatherOnHour1List2.temp = 10
                        let weatherOnHour2List2  = WeatherOnHour(for: location, at: date1AugAnd16Hours2)
                        weatherOnHour2List2.temp = 10
                        let weatherOnHour3List2  = WeatherOnHour(for: location, at: date1AugAnd17Hours2)
                        weatherOnHour3List2.temp = 10
                        let weatherOnHour4List2  = WeatherOnHour(for: location, at: date1AugAnd18Hours2)
                        weatherOnHour4List2.temp = 10
                        let weatherOnHour5List2  = WeatherOnHour(for: location, at: date1AugAnd19Hours2)
                        weatherOnHour5List2.temp = 10
                        let weatherOnHour6List2  = WeatherOnHour(for: location, at: date1AugAnd20Hours2)
                        weatherOnHour6List2.temp = 10
                        
                        
                        // add test data to random position
                        weatherOnHours1.append(weatherOnHour1List1)
                        weatherOnHours1.append(weatherOnHour2List1)
                        weatherOnHours1.append(weatherOnHour3List1)
                        weatherOnHours1.append(weatherOnHour4List1)
                        weatherOnHours1.append(weatherOnHour5List1)
                        weatherOnHours1.append(weatherOnHour6List1)
                        
                        weatherOnHours2.append(weatherOnHour1List2)
                        weatherOnHours2.append(weatherOnHour2List2)
                        weatherOnHours2.append(weatherOnHour3List2)
                        weatherOnHours2.append(weatherOnHour4List2)
                        weatherOnHours2.append(weatherOnHour5List2)
                        weatherOnHours2.append(weatherOnHour6List2)
                        
                        let diffWeatherOnHours = WeatherEngine.findTemperatureDiff(forWeatherOnHour: weatherOnHours2, andWeatherOnHours: weatherOnHours1)
                        
                        XCTAssertNotNil(diffWeatherOnHours.0, "diffWeatherOnHours.0 is null")
                        XCTAssertNotNil(diffWeatherOnHours.1, "diffWeatherOnHours.1 is null")
                        
                        guard let date1 = diffWeatherOnHours.0 else {
                            testExpectation.fulfill()
                            return
                        }
                        
                        guard let date2 = diffWeatherOnHours.1 else {
                            testExpectation.fulfill()
                            return
                        }
                        
                        XCTAssert(date1.date! == date1AugAnd18Hours1 || date2.date! == date1AugAnd18Hours1, "\(date1.date!.toString()) != \(date1AugAnd18Hours1.toString()) && \(date2.date!.toString()) != \(date1AugAnd18Hours1.toString())")
                        testExpectation.fulfill()
                        
        },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
        })
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
        
    }
    
    func testWeatherEngineGroupWeatherOnHour() {
        
        let coord = CLLocationCoordinate2D(latitude: 56.817439, longitude: 60.631440)
        let testExpectation = expectation(description: "Did finish operation expectation")
        
        Location.make(withLocation: coord,
                      onCompletion: {(location)  in
                        
                        var weatherOnHours : Array<WeatherOnHour> = []
                        
                        let date1AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 15, minut: 00, second: 00)!
                        let date1AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 16, minut: 00, second: 00)!
                        let date1AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 1, hour: 17, minut: 00, second: 00)!
                        
                        let date1Aug = date1AugAnd15Hours.dateWithoutTime()
                        
                        let date2AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 15, minut: 00, second: 00)!
                        let date2AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 16, minut: 00, second: 00)!
                        let date2AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 2, hour: 17, minut: 00, second: 00)!
                        
                        let date2Aug = date2AugAnd15Hours.dateWithoutTime()
                        
                        let date3AugAnd15Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 15, minut: 00, second: 00)!
                        let date3AugAnd16Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 16, minut: 00, second: 00)!
                        let date3AugAnd17Hours = Date.make(withYear: 2017, month: 08, day: 3, hour: 17, minut: 00, second: 00)!
                        
                        let date3Aug = date3AugAnd15Hours.dateWithoutTime()
                        
                        // add test data to random position
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd15Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd15Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd16Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd16Hours))
                        
                        weatherOnHours.append(WeatherOnHour(for: location, at: date2AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date1AugAnd17Hours))
                        weatherOnHours.append(WeatherOnHour(for: location, at: date3AugAnd15Hours))
                        
                        WeatherEngine.group(weatherOnHour: weatherOnHours, onCompletion: { (days, hoursByDay) in
                            XCTAssert(days[0] == date1Aug, "days[0] (\(days[0])) != \(date1Aug)")
                            let weatherList0 = hoursByDay[0]
                            XCTAssert(weatherList0[0].date! == date1AugAnd15Hours, "weatherList0[0].date (\(weatherList0[0].date!)) != \(date1AugAnd15Hours)")
                            XCTAssert(weatherList0[1].date! == date1AugAnd16Hours, "weatherList0[1].date (\(weatherList0[1].date!)) != \(date1AugAnd16Hours)")
                            XCTAssert(weatherList0[2].date! == date1AugAnd17Hours, "weatherList0[2].date (\(weatherList0[2].date!)) != \(date1AugAnd17Hours)")
                            
                            XCTAssert(days[1] == date2Aug, "days[1] (\(days[1])) != \(date2Aug)")
                            let weatherList1 = hoursByDay[1]
                            XCTAssert(weatherList1[0].date! == date2AugAnd15Hours, "weatherList1[0].date (\(weatherList1[0].date!)) != \(date2AugAnd15Hours)")
                            XCTAssert(weatherList1[1].date! == date2AugAnd16Hours, "weatherList1[1].date (\(weatherList1[1].date!)) != \(date2AugAnd16Hours)")
                            XCTAssert(weatherList1[2].date! == date2AugAnd17Hours, "weatherList1[2].date (\(weatherList1[2].date!)) != \(date2AugAnd17Hours)")
                            
                            XCTAssert(days[2] == date3Aug, "days[2] (\(days[2])) != \(date3Aug)")
                            let weatherList2 = hoursByDay[2]
                            XCTAssert(weatherList2[0].date! == date3AugAnd15Hours, "weatherList2[0].date (\(weatherList2[0].date!)) != \(date3AugAnd15Hours)")
                            XCTAssert(weatherList2[1].date! == date3AugAnd16Hours, "weatherList2[1].date (\(weatherList2[1].date!)) != \(date3AugAnd16Hours)")
                            XCTAssert(weatherList2[2].date! == date3AugAnd17Hours, "weatherList2[2].date (\(weatherList2[2].date!)) != \(date3AugAnd17Hours)")
                            
                            testExpectation.fulfill()
                        })
                        
                      },
                      onError: { (message) in
                        XCTAssert(false, "errorMessage=\(message)")
                        testExpectation.fulfill()
                      })
        
        waitForExpectations(timeout: performanceTimeout, handler: nil)
        
    }
    
    
    //    func testPerformanceExample() {
    //        // This is an example of a performance test case.
    //        self.measure {
    //            // Put the code you want to measure the time of here.
    //        }
    //    }
    
}
