//
//  WeatherOnDay.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation

class WeatherOnDay: NSObject {
    
    public private(set) var location : Location? = nil
    public private(set) var date : Date? = nil
    var sunset     : Date? = nil   // время заката
    var sunrise    : Date? = nil   // время восхода
    var temp       : Double? = nil // текущая температура
    var tempMin    : Double? = nil // минимальная температура за сутки
    var tempMax    : Double? = nil // максимальная температура за сутки
    var pressure   : Int? = nil    // атмосферное давление
    var humidity   : Int? = nil    // влажность воздуха
    var visibility : Int? = nil    // видимость
    var windSpeed  : Int? = nil    // скорость ветра
    var windDeg    : Int? = nil    // направление ветра
    var clouds     : Int? = nil    // облачность
    var rain       : Double? = nil // Уровень осадков
    var icon       : String? = nil // описание
    var detail     : String? = nil // детальное описание


    init(for weatherLocation: Location, at weatherDate: Date) {
        super.init()
        self.location = weatherLocation
        self.date     = weatherDate
    }

}
