//
//  HTTPEngine.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation

class HTTPEngine: NSObject {
    
    private var session : URLSession? = nil
    private var request : URLRequest? = nil
    
    static let shared: HTTPEngine = {
        let instance = HTTPEngine()
        return instance
    }()
    
    private override init() {
        super.init()
        session = URLSession.shared
    }
    
    private static func httpRequest(path: String, contentType: String, httpMethod: String, parameters : [String: Any]?, onCompletion: @escaping (Int, [String: Any]?, [[String: Any]]?, Error?) -> Void) {
        
        if self.shared.session != nil {
            
            if let url = URL(string: path) {
                var request : URLRequest = URLRequest(url: url)
                
                request.httpMethod = httpMethod
                
                
                request.setValue(contentType, forHTTPHeaderField: "Content-Type")
                request.setValue(contentType, forHTTPHeaderField: "Accept")
                request.setValue("utf-8", forHTTPHeaderField: "charset")

                
                
                if (contentType == "application/x-www-form-urlencoded")
                {
                    var parameterList : String = ""
                    let parameterKeys = Array(parameters!.keys)
                    let parameterValues = Array(parameters!.values)
                    
                    for index in (0 ..< parameters!.count) {
                        if (index > 0) {
                            parameterList += "&"
                        }
                        parameterList += "\(parameterKeys[index])=\(parameterValues[index])"
                    }
                    
                    var postData = parameterList.data(using: String.Encoding.ascii, allowLossyConversion: true)!
                    let postLength = "\(postData.count)"
                    request.httpBody = postData
                    request.setValue(postLength, forHTTPHeaderField: "Content-Length")
                    
                } else if (contentType == "application/json") {
                    if let parameters = parameters { // parameters for POST, DELETE, UPDATE
                        do {
                            let data = try JSONSerialization.data(withJSONObject: parameters, options: JSONSerialization.WritingOptions())
                            request.httpBody = data
                        } catch {
                            onCompletion(0, nil, nil, error as NSError)
                            return
                        }
                    }
                }
                
                let task = self.shared.session?.dataTask(with: request) {(data, response, error) -> Void in
                    if let httpResponse = response as? HTTPURLResponse {
                        let statusCode = httpResponse.statusCode

                        if error != nil {
                            onCompletion(statusCode, nil, nil, error)
                        } else {
                            if let data = data {
                                do {
                                    if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [String: Any]
                                    {
                                        onCompletion(statusCode, json, nil, error)
                                    }
                                    else if let json = try JSONSerialization.jsonObject(with: data, options: .allowFragments) as? [[String: Any]]
                                    {
                                        onCompletion(statusCode, nil, json, error)
                                    }
                                } catch {
                                    onCompletion(statusCode, nil, nil, error)
                                }
                            } else {
                                onCompletion(statusCode, nil, nil, error)
                            }
                        }
                    }
                    else
                    {
                        onCompletion(0, nil, nil, error)
                    }
                }
                
                task?.resume()
            }
        }
    }
    
    
    
    static func httpGetRequest(path: String, parameters : [String: Any]?, onCompletion: @escaping (Int, [String: Any]?, [[String: Any]]?, Error?) -> Void) {
        
        var requestPath : String = path
        let contentType: String = "application/json"
        
        if let parameters = parameters  {
            var parameterList : String = ""
            let parameterKeys = Array(parameters.keys)
            let parameterValues = Array(parameters.values)
            
            for index in (0 ..< parameters.count) {
                if (index > 0) {
                    parameterList += "&"
                }
                parameterList += "\(parameterKeys[index])=\(parameterValues[index])"
            }
            
            requestPath = requestPath + "?" + parameterList
        }
        
        HTTPEngine.httpRequest(path: requestPath, contentType: contentType, httpMethod: "GET", parameters : nil, onCompletion: onCompletion)
    }
    
}
