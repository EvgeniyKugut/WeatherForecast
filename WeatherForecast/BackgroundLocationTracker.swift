//
//  BackgroundLocationTracker.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit

class BackgroundLocationTracker : NSObject, CLLocationManagerDelegate {
        
    var locationManager : CLLocationManager!
    
    public private(set) var location : Location?
    
    static let shared: BackgroundLocationTracker = {
        let instance = BackgroundLocationTracker()
        return instance
    }()
    
    override init() {
        super.init()
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        self.locationManager.pausesLocationUpdatesAutomatically = true
        
    }
    
    static func start() {
        BackgroundLocationTracker.shared.startUpdates()
    }
    
    static func stop() {
        BackgroundLocationTracker.shared.stopUpdates()
    }
    
    func stopUpdates() {
        self.locationManager.stopUpdatingLocation()
        self.locationManager.allowsBackgroundLocationUpdates = false
    }
    
    func startUpdates() {
        
        self.locationManager.requestAlwaysAuthorization()
        self.locationManager.allowsBackgroundLocationUpdates = true
        self.locationManager.startUpdatingLocation()
    }
    
    static func updateRequest() {
        LocationTracker.shared.locationManager.requestAlwaysAuthorization()
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        guard let location = manager.location else {
            self.location = nil
            return
        }
        
        Location.make(withLocation: location.coordinate,
                      onCompletion: {(makedLocation)  in
                        self.location = makedLocation

                        WeatherEngine.updateWeatherWithCheckChanges(onHasChanges: {(message) in
                                                                        LocalPushNotificator.push(message: message)
                                                                    },
                                                                    onNoChanges: {() in
                                                                    },
                                                                    onError: {(message) in
                                                                    })
                        
                     },
                      onError: { (message) in
                        self.location = nil
                     })
    }
    
}
