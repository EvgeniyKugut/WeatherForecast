//
//  WaitFinishWeatherRequeryCell.swift
//  WeatherForecast
//
//  Created by Evgeniy on 03.09.17.
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class WaitFinishWeatherRequeryCell: UITableViewCell {

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

    }

}
