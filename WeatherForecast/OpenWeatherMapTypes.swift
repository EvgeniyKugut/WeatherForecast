//
//  OpenWeatherMapTypes.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation

enum OpenWeatherMapWheaterType {
    case thunderstorm
    case drizzle
    case rain
    case snow
    case atmosphere
    case clear
    case clouds
    case extreme
    case additional
    case undefined
}

enum OpenWeatherMapWheaterSubType {
    case thunderstormWithLightRain
    case thunderstormWithRain
    case thunderstormWithHeavyRain
    case lightThunderstorm
    case thunderstorm
    case heavyThunderstorm
    case raggedThunderstorm
    case thunderstormWithLightDrizzle
    case thunderstormWithDrizzle
    case thunderstormWithHeavyDrizzle
    case lightIntensityDrizzle
    case drizzle
    case heavyIntensityDrizzle
    case lightIntensityDrizzleRain
    case drizzleRain
    case heavyIntensityDrizzleRain
    case showerRainAndDrizzle
    case heavyShowerRainAndDrizzle
    case showerDrizzle
    case lightRain
    case moderateRain
    case heavyIntensityRain
    case veryHeavyRain
    case extremeRain
    case freezingRain
    case lightIntensityShowerRain
    case showerRain
    case heavyIntensityShowerRain
    case raggedShowerRain
    case lightSnow
    case snow
    case heavySnow
    case sleet
    case showerSleet
    case lightRainAndSnow
    case rainAndSnow
    case lightShowerSnow
    case showerSnow
    case heavyShowerSnow
    case mist
    case smoke
    case haze
    case sandDustWhirls
    case fog
    case sand
    case dust
    case volcanicAsh
    case squalls
    case tornado
    case clearSky
    case fewClouds
    case scatteredClouds
    case brokenClouds
    case overcastClouds
    case tornadoExtreme
    case tropicalStorm
    case hurricaneExtreme
    case cold
    case hot
    case windy
    case hail
    case calm
    case lightBreeze
    case gentleBreeze
    case moderateBreeze
    case freshBreeze
    case strongBreeze
    case highWindNearGale
    case gale
    case severeGale
    case storm
    case violentStorm
    case hurricane
    case undefined
}




func getPartsOfDay(byIdentifyer iconIdentifyer: String) -> PartsOfDay {
    
    let strPartsOfDay : String = iconIdentifyer.substring(from: iconIdentifyer.index(iconIdentifyer.startIndex, offsetBy: 2))
    
    switch (strPartsOfDay) {
    case "d":
        return .afternoon
    case "n":
        return .night
    default:
        return .undefined
    }
}




func getOpenWeatherMapWheaterType(byIdentifyer identifyer: Int) -> OpenWeatherMapWheaterSubType {
    
    switch (identifyer) {
    case 200:
        return .thunderstormWithLightRain
    case 201:
        return .thunderstormWithRain
    case 202:
        return .thunderstormWithHeavyRain
    case 210:
        return .lightThunderstorm
    case 211:
        return .thunderstorm
    case 212:
        return .heavyThunderstorm
    case 221:
        return .raggedThunderstorm
    case 230:
        return .thunderstormWithLightDrizzle
    case 231:
        return .thunderstormWithDrizzle
    case 232:
        return .thunderstormWithHeavyDrizzle
    case 300:
        return .lightIntensityDrizzle
    case 301:
        return .drizzle
    case 302:
        return .heavyIntensityDrizzle
    case 310:
        return .lightIntensityDrizzleRain
    case 311:
        return .drizzleRain
    case 312:
        return .heavyIntensityDrizzleRain
    case 313:
        return .showerRainAndDrizzle
    case 314:
        return .heavyShowerRainAndDrizzle
    case 321:
        return .showerDrizzle
    case 500:
        return .lightRain
    case 501:
        return .moderateRain
    case 502:
        return .heavyIntensityRain
    case 503:
        return .veryHeavyRain
    case 504:
        return .extremeRain
    case 511:
        return .freezingRain
    case 520:
        return .lightIntensityShowerRain
    case 521:
        return .showerRain
    case 522:
        return .heavyIntensityShowerRain
    case 531:
        return .raggedShowerRain
    case 600:
        return .lightSnow
    case 601:
        return .snow
    case 602:
        return .heavySnow
    case 611:
        return .sleet
    case 612:
        return .showerSleet
    case 615:
        return .lightRainAndSnow
    case 616:
        return .rainAndSnow
    case 620:
        return .lightShowerSnow
    case 621:
        return .showerSnow
    case 622:
        return .heavyShowerSnow
    case 701:
        return .mist
    case 711:
        return .smoke
    case 721:
        return .haze
    case 731:
        return .sandDustWhirls
    case 741:
        return .fog
    case 751:
        return .sand
    case 761:
        return .dust
    case 762:
        return .volcanicAsh
    case 771:
        return .squalls
    case 781:
        return .tornado
    case 800:
        return .clearSky
    case 801:
        return .fewClouds
    case 802:
        return .scatteredClouds
    case 803:
        return .brokenClouds
    case 804:
        return .overcastClouds
    case 900:
        return .tornadoExtreme
    case 901:
        return .tropicalStorm
    case 902:
        return .hurricaneExtreme
    case 903:
        return .cold
    case 904:
        return .hot
    case 905:
        return .windy
    case 906:
        return .hail
    case 951:
        return .calm
    case 952:
        return .lightBreeze
    case 953:
        return .gentleBreeze
    case 954:
        return .moderateBreeze
    case 955:
        return .freshBreeze
    case 956:
        return .strongBreeze
    case 957:
        return .highWindNearGale
    case 958:
        return .gale
    case 959:
        return .severeGale
    case 960:
        return .storm
    case 961:
        return .violentStorm
    case 962:
        return .hurricane
    default:
        return .undefined
    }
}

func getWeatherDetailDescription(byOpenWeatherMapWheaterSubType openWeatherMapWheaterSubType: OpenWeatherMapWheaterSubType) -> String? {
    
    switch (openWeatherMapWheaterSubType) {
    case .thunderstormWithLightRain:
        return "thunderstorm with light rain"
    case .thunderstormWithRain:
        return "thunderstorm with rain"
    case .thunderstormWithHeavyRain:
        return "thunderstorm with heavy rain"
    case .lightThunderstorm:
        return "light thunderstorm"
    case .thunderstorm:
        return "thunderstorm"
    case .heavyThunderstorm:
        return "heavy thunderstorm"
    case .raggedThunderstorm:
        return "ragged thunderstorm"
    case .thunderstormWithLightDrizzle:
        return "thunderstorm with light drizzle"
    case .thunderstormWithDrizzle:
        return "thunderstorm with drizzle"
    case .thunderstormWithHeavyDrizzle:
        return "thunderstorm with heavy drizzle"
    case .lightIntensityDrizzle:
        return "light intensity drizzle"
    case .drizzle:
        return "drizzle"
    case .heavyIntensityDrizzle:
        return "heavy intensity drizzle"
    case .lightIntensityDrizzleRain:
        return "light intensity drizzle rain"
    case .drizzleRain:
        return "drizzle rain"
    case .heavyIntensityDrizzleRain:
        return "heavy intensity drizzle rain"
    case .showerRainAndDrizzle:
        return "shower rain and drizzle"
    case .heavyShowerRainAndDrizzle:
        return "heavy shower rain and drizzle"
    case .showerDrizzle:
        return "shower drizzle"
    case .lightRain:
        return "light rain"
    case .moderateRain:
        return "moderate rain"
    case .heavyIntensityRain:
        return "heavy intensity rain"
    case .veryHeavyRain:
        return "very heavy rain"
    case .extremeRain:
        return "extreme rain"
    case .freezingRain:
        return "freezing rain"
    case .lightIntensityShowerRain:
        return "light intensity shower rain"
    case .showerRain:
        return "shower rain"
    case .heavyIntensityShowerRain:
        return "heavy intensity shower rain"
    case .raggedShowerRain:
        return "ragged shower rain"
    case .lightSnow:
        return "light snow"
    case .snow:
        return "snow"
    case .heavySnow:
        return "heavy snow"
    case .sleet:
        return "sleet"
    case .showerSleet:
        return "shower sleet"
    case .lightRainAndSnow:
        return "light rain and snow"
    case .rainAndSnow:
        return "rain and snow"
    case .lightShowerSnow:
        return "light shower snow"
    case .showerSnow:
        return "shower snow"
    case .heavyShowerSnow:
        return "heavy shower snow"
    case .mist:
        return "mist"
    case .smoke:
        return "smoke"
    case .haze:
        return "haze"
    case .sandDustWhirls:
        return "sand, dust whirls"
    case .fog:
        return "fog"
    case .sand:
        return "sand"
    case .dust:
        return "dust"
    case .volcanicAsh:
        return "volcanic ash"
    case .squalls:
        return "squalls"
    case .tornado:
        return "tornado"
    case .clearSky:
        return "clear sky"
    case .fewClouds:
        return "few clouds"
    case .scatteredClouds:
        return "scattered clouds"
    case .brokenClouds:
        return "broken clouds"
    case .overcastClouds:
        return "overcast clouds"
    case .tornadoExtreme:
        return "tornado"
    case .tropicalStorm:
        return "tropical storm"
    case .hurricaneExtreme:
        return "hurricane"
    case .cold:
        return "cold"
    case .hot:
        return "hot"
    case .windy:
        return "windy"
    case .hail:
        return "hail"
    case .calm:
        return "calm"
    case .lightBreeze:
        return "light breeze"
    case .gentleBreeze:
        return "gentle breeze"
    case .moderateBreeze:
        return "moderate breeze"
    case .freshBreeze:
        return "fresh breeze"
    case .strongBreeze:
        return "strong breeze"
    case .highWindNearGale:
        return "high wind, near gale"
    case .gale:
        return "gale"
    case .severeGale:
        return "severe gale"
    case .storm:
        return "storm"
    case .violentStorm:
        return "violent storm"
    case .hurricane:
        return "hurricane"
    default:
        return nil
    }
    
    
}

func getOpenWeatherMapWheaterType(byOpenWeatherMapWheaterSubType openWeatherMapWheaterSubType: OpenWeatherMapWheaterSubType) -> OpenWeatherMapWheaterType {
    
    switch (openWeatherMapWheaterSubType) {
    case .thunderstormWithLightRain, .thunderstormWithRain, .thunderstormWithHeavyRain, .lightThunderstorm, .thunderstorm, .heavyThunderstorm, .raggedThunderstorm, .thunderstormWithLightDrizzle, .thunderstormWithDrizzle, .thunderstormWithHeavyDrizzle:
        return .thunderstorm
    case .lightIntensityDrizzle, .drizzle, .heavyIntensityDrizzle, .lightIntensityDrizzleRain, .drizzleRain, .heavyIntensityDrizzleRain, .showerRainAndDrizzle, .heavyShowerRainAndDrizzle, .showerDrizzle:
        return .drizzle
    case .lightRain, .moderateRain, .heavyIntensityRain, .veryHeavyRain, .extremeRain, .freezingRain, .lightIntensityShowerRain, .showerRain, .heavyIntensityShowerRain, .raggedShowerRain:
        return .rain
    case .lightSnow, .snow, .heavySnow, .sleet, .showerSleet, .lightRainAndSnow, .rainAndSnow, .lightShowerSnow, .showerSnow, .heavyShowerSnow:
        return .snow
    case .mist, .smoke, .haze, .sandDustWhirls, .fog, .sand, .dust, .volcanicAsh, .squalls, .tornado:
        return .atmosphere
    case .clearSky:
        return .clear
    case .fewClouds, .scatteredClouds, .brokenClouds, .overcastClouds:
        return .clouds
    case .tornadoExtreme, .tropicalStorm, .hurricaneExtreme, .cold, .hot, .windy, .hail:
        return .extreme
    case .calm, .lightBreeze, .gentleBreeze, .moderateBreeze, .freshBreeze, .strongBreeze, .highWindNearGale, .gale, .severeGale, .storm, .violentStorm, .hurricane:
        return .additional
    default:
        return .undefined
    }
    
    
}

func getIcon(byIdentifyer iconIdentifyer: String) -> String? {
    
    switch iconIdentifyer {
    case "01n":
        return "fullmoon"
    case "02n":
        return "moon_cloud"
    case "03n":
        return "medium_cloud"
    case "04n":
        return "max_cloud"
    case "09n":
        return "rain"
    case "10n":
        return "night_rain"
    case "11n":
        return "night_thunder_rain"
    case "13n":
        return "snow"
    case "50n":
        return "night_fog"
    case "01d":
        return "sun.png"
    case "02d":
        return "cloud_sun.png"
    case "03d":
        return "medium_cloud.png"
    case "04d":
        return "max_cloud.png"
    case "09d":
        return "rain.png"
    case "10d":
        return "sun_medium_rain.png"
    case "11d":
        return "sun_thunder_rain.png"
    case "13d":
        return "snow.png"
    case "50d":
        return "fog.png"
    default:
        return nil
    }
    
}


func getIcon(byWehatherSubType: OpenWeatherMapWheaterSubType, forPartsOfDay partsOfDay: PartsOfDay) -> String? {

    switch byWehatherSubType {
    case .thunderstormWithLightRain:    // "гроза с небольшим дождем
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .thunderstormWithRain:          // гроза с дождем
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .thunderstormWithHeavyRain:    // гроза с сильным дождем
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .lightThunderstorm:              // слабая гроза
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .thunderstorm:                    // гроза
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .heavyThunderstorm:              // сильная гроза
        if partsOfDay == .night {
            return "cloud_thunder_rain.png"
        } else {
            return "cloud_thunder_rain.png"
        }
    case .raggedThunderstorm:             // шквальная гроза
        if partsOfDay == .night {
            return "cloud_thunder_rain.png"
        } else {
            return "cloud_thunder_rain.png"
        }
    case .thunderstormWithLightDrizzle: // Гроза с легкой изморосью
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .thunderstormWithDrizzle:       // Гроза с изморосью
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }
    case .thunderstormWithHeavyDrizzle: // Гроза с сильной изморосью
        if partsOfDay == .night {
            return "night_thunder_rain.png"
        } else {
            return "sun_thunder_rain.png"
        }

    case .lightIntensityDrizzle:         // слабая изморось
        if partsOfDay == .night {
            return "night_flurry.png"
        } else {
            return "sun_flurrie.png"
        }
    case .drizzle:                         // изморось
        if partsOfDay == .night {
            return "night_flurry.png"
        } else {
            return "sun_flurrie.png"
        }
    case .heavyIntensityDrizzle:         // сильная изморось
        if partsOfDay == .night {
            return "night_flurry.png"
        } else {
            return "sun_flurrie.png"
        }
    case .lightIntensityDrizzleRain:    // слабая изморось с дождем
        if partsOfDay == .night {
            return "night_flurry.png"
        } else {
            return "sun_flurrie.png"
        }
    case .drizzleRain:                    // изморось с дождем
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .heavyIntensityDrizzleRain:    // сильная изморось с дождем
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .showerRainAndDrizzle:         // проливной дождь и изморось
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .heavyShowerRainAndDrizzle:   // сильный ливень и изморось
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .showerDrizzle:                  // ливень и изморось
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }

    case .lightRain:                      // небольшой дождь
        if partsOfDay == .night {
            return "night_rain.png"
        } else {
            return "sun_and_rain.png"
        }
    case .moderateRain:                   // умеренный дождь
        if partsOfDay == .night {
            return "medium_rain.png"
        } else {
            return "sun_medium_rain.png"
        }
    case .heavyIntensityRain:            // интенсивный дождь
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }
    case .veryHeavyRain:                 // очень сильный дождь
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }
    case .extremeRain:                    // экстремальный дождь
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }
    case .freezingRain:                   // леденящий дождь
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .lightIntensityShowerRain:       // проливной дождь
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }
    case .showerRain:                     // ливень
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }
    case .heavyIntensityShowerRain:     // сильный ливень
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }
    case .raggedShowerRain:              // шквальный ливень
        if partsOfDay == .night {
            return "rain.png"
        } else {
            return "rain.png"
        }

    case .lightSnow:                      // небольшой снег
        if partsOfDay == .night {
            return "night_and_snow.png"
        } else {
            return "sun_and_snow.png"
        }
        
    case .snow:                            // снег
        if partsOfDay == .night {
            return "snow.png"
        } else {
            return "sun_medium_snow.png"
        }
    case .heavySnow:                      // сильный снегопад
        if partsOfDay == .night {
            return "much_snow.png"
        } else {
            return "much_snow.png"
        }
    case .sleet:                           // дождь со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .showerSleet:                    // ливень со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .lightRainAndSnow:               // легкий дождь со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .rainAndSnow:                   // дождь со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .lightShowerSnow:               // слабый ливень со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .showerSnow:                     // ливень со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
    case .heavyShowerSnow:               // сильный ливень со снегом
        if partsOfDay == .night {
            return "rain_and_snow.png"
        } else {
            return "rain_and_snow.png"
        }
        
    case .mist:                            // дымка
        if partsOfDay == .night {
            return "night_fog.png"
        } else {
            return "fog.png"
        }
    case .smoke:                           // смог
        if partsOfDay == .night {
            return "night_fog.png"
        } else {
            return "fog.png"
        }
    case .haze:                            // мгла
        if partsOfDay == .night {
            return "night_fog.png"
        } else {
            return "fog.png"
        }
    case .sandDustWhirls:                  // песчанные и пыльные бури
        if partsOfDay == .night {
            return "night_fog.png"
        } else {
            return "fog.png"
        }
    case .fog:                             // туман
        if partsOfDay == .night {
            return "fog.png"
        } else {
            return "fog.png"
        }
    case .sand:                            // песок
        if partsOfDay == .night {
            return "fog.png"
        } else {
            return "fog.png"
        }
    case .dust:                            // пыль
        if partsOfDay == .night {
            return "fog.png"
        } else {
            return "fog.png"
        }
    case .volcanicAsh:                    // вулканический пепел
        if partsOfDay == .night {
            return "fog.png"
        } else {
            return "fog.png"
        }
    case .squalls:                         // шквал
        if partsOfDay == .night {
            return "cloud.png"
        } else {
            return "cloud.png"
        }
    case .tornado:                         // торнадо
        if partsOfDay == .night {
            return "cloud.png"
        } else {
            return "cloud.png"
        }
        
        
        
        

    case .clearSky:                       // ясное небо
        if partsOfDay == .night {
            return "fullmoon.png"
        } else {
            return "sun.png"
        }

        

    
    case .fewClouds:                      // слабая облачность
        if partsOfDay == .night {
            return "moon_cloud.png"
        } else {
            return "sun_minimal_clouds.png"
        }
    case .scatteredClouds:                // рассеянные облака
        if partsOfDay == .night {
            return "moon_cloud_medium.png"
        } else {
            return "sun_maximum_clouds.png"
        }
    case .brokenClouds:                   // облачность
        if partsOfDay == .night {
            return "moon_cloud_medium.png"
        } else {
            return "sun_most_cloudy.png"
        }
    case .overcastClouds:                 // высокая облачность
        if partsOfDay == .night {
            return "overcast.png"
        } else {
            return "overcast.png"
        }

    case .tornadoExtreme:                         // торнадо
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .tropicalStorm:                  // тропический шторм
        if partsOfDay == .night {
            return "cloud_thunder_rain.png"
        } else {
            return "cloud_thunder_rain.png"
        }
    case .hurricane:                       // буря
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .cold:                            // холодно
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .hot:                             // жарко
        if partsOfDay == .night {
            return nil
        } else {
            return "sun.png"
        }
    case .windy:                           // ветренно
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .hail:                            // град
        if partsOfDay == .night {
            return "medium_ice.png"
        } else {
            return "medium_ice.png"
        }

    case .calm:                            // спокойный
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .lightBreeze:                    // легкий ветерок
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .gentleBreeze:                   // нежный бриз
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .moderateBreeze:                 // умеренный ветерок
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .freshBreeze:                    // свежий ветерок
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .strongBreeze:                   // ветер
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .highWindNearGale:            // сильный ветер, близко к шторму
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .gale:                            // шторм
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .severeGale:                     // сильный шторм
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .storm:                           // шторм
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .violentStorm:                   // сильный шторм
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    case .hurricaneExtreme:                       // ураган
        if partsOfDay == .night {
            return nil
        } else {
            return nil
        }
    default:
        return nil
    }
}
