//
//  OpenWeatherMap.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation
import CoreLocation


let openWeatherMapApiKey  : String = "f5500334c1595a362f08ef32af6323a0"
let openWeatherMapApiHost : String = "https://api.openweathermap.org"

class OpenWeatherMap: NSObject {
    
    // Call current weather data for one location By geographic coordinates
    //api.openweathermap.org/data/2.5/weather?lat=35&lon=139
    
/*
    {
        "coord": {
            "lon": 60.59,
            "lat": 56.9
        },
        "weather": [
        {
        "id": 803, // * идентификатор детального описания погоды
        "main": "Clouds", // описание погоды
        "description": "broken clouds", // * детальное описание погоды
        "icon": "04n" // * иконка погоды по умолчанию
        }
        ],
        "base": "stations",
        "main": {
            "temp": 286.15,     // текущая температура
            "pressure": 1018,   // давление
            "humidity": 100,    // влажность
            "temp_min": 286.15, // минимальная температура
            "temp_max": 286.15  // максимальная температура
        },
        "visibility": 10000, // видимость
        "wind": {
            "speed": 2, // скорость ветра
            "deg": 10   // направление (градус)
        },
        "clouds": {
            "all": 75 // облачность
        },
        "dt": 1504108800, // * текущее время/дата
        "sys": {
            "type": 1,
            "id": 7318,
            "message": 0.02,
            "country": "RU",
            "sunrise": 1504054633, // * восход
            "sunset": 1504105049   // * закат
        },
        "id": 1486209,
        "name": "Yekaterinburg",
        "cod": 200 // код состояния ответа
       }
*/
    
    func getCurrentWeatherData(forOne location : Location,
                               onCompletion: @escaping (WeatherOnDay) -> Void,
                               onError: @escaping (Int, String) -> Void) {
        
        
        guard let coordinate = location.coordinate else {
            onError(-1, "The location coordinate is not defined")
            return
        }
        
        
        let units : String
        if SettingsApp.getTemperatureUnits() == .imperial {
            units = "imperial"
        } else {
            units = "metric"
        }
        
        let parameters = ["lat": coordinate.latitude, "lon": coordinate.longitude, "APPID": openWeatherMapApiKey, "units": units] as [String : Any]
        HTTPEngine.httpGetRequest(path: openWeatherMapApiHost + "/data/2.5/weather", parameters: parameters, onCompletion: { (code, json, jsonArray, error) in

            guard let jsonResponse = json else {
                if let error = error {
                    onError(code, error.localizedDescription)
                }
                return
            }
            
            if let cod = jsonResponse["cod"] as? Int {
                if cod == 200 {
                    
                    
                    guard  let dt = jsonResponse["dt"] as? Double else {
                        return
                    }
                    
                    let currentWeather = WeatherOnDay(for: location,  at: Date(timeIntervalSince1970: dt))
                    
                    if let mainWeather = jsonResponse["main"] as? [String : Any] {
                        if let temp = mainWeather["temp"] as? Double {
                            currentWeather.temp = temp
                        }
                        
                        if let temp_min = mainWeather["temp_min"] as? Double {
                            currentWeather.tempMin = temp_min
                        }

                        if let temp_max = mainWeather["temp_max"] as? Double {
                            currentWeather.tempMax = temp_max
                        }
                        
                        if let pressure = mainWeather["pressure"] as? Int {
                            currentWeather.pressure = pressure
                        }

                        if let humidity = mainWeather["humidity"] as? Int {
                            currentWeather.humidity = humidity
                        }
                        
                    }
                    
                    if let visibility = jsonResponse["visibility"] as? Int {
                        currentWeather.visibility = visibility
                    } else {
                        print("not visibility")
                    }
                    
                    if let wind = jsonResponse["wind"] as? [String : Any] {
                        if let windSpeed = wind["speed"] as? Int {
                            currentWeather.windSpeed = windSpeed
                        }
                        
                        if let windDeg = wind["deg"] as? Double {
                            currentWeather.windDeg = Int(windDeg)
                        }
                    }

                    if let clouds = jsonResponse["clouds"] as? [String : Any] {
                        if let cloudsPercent = clouds["all"] as? Int {
                            currentWeather.clouds = cloudsPercent
                        }
                    }
                    
                    if let rain = jsonResponse["rain"] as? [String : Any] {
                        if let rain3h = rain["3h"] as? Double {
                            currentWeather.rain = rain3h
                        }
                    }
                    
                    if let weathers = jsonResponse["weather"] as? [[String : Any]] {
                    
                        for weather in (weathers) {
                            var partsOfDay : PartsOfDay = .afternoon
                            if let weatherIcon = weather["icon"] as? String {
                                if let icon = getIcon(byIdentifyer: weatherIcon) {
                                    currentWeather.icon = icon
                                }
                                partsOfDay = getPartsOfDay(byIdentifyer: weatherIcon)
                            }
                            
                            if let weatherIdentifyer = weather["id"] as? Int {
                                let openWeatherMapWheaterSubType : OpenWeatherMapWheaterSubType = getOpenWeatherMapWheaterType(byIdentifyer: weatherIdentifyer)
                                
                                if let detailDescription = getWeatherDetailDescription(byOpenWeatherMapWheaterSubType: openWeatherMapWheaterSubType) {
                                    currentWeather.detail = detailDescription
                                } else if let description = weather["description"] as? String {
                                    currentWeather.detail = description
                                }
                                
                                if let icon = getIcon(byWehatherSubType: openWeatherMapWheaterSubType, forPartsOfDay: partsOfDay) {
                                    currentWeather.icon = icon
                                }
                                
                            } else if let description = weather["description"] as? String {
                                currentWeather.detail = description
                            }
                        }

                    }
                    
                    if let sys = jsonResponse["sys"] as? [String : Any] {
                        
                        if let sunrise = sys["sunrise"] as? Double {
                            currentWeather.sunrise = Date(timeIntervalSince1970: sunrise)
                        }
                        
                        if let sunset = sys["sunset"] as? Double {
                            currentWeather.sunset = Date(timeIntervalSince1970: sunset)
                        }
                        
                    }
                    
                    onCompletion(currentWeather)
                    
                } else {
                    guard let errorMessage = jsonResponse["message"] as? String else {
                        if let error = error {
                            onError(code, error.localizedDescription)
                        }
                        return
                    }
                    onError(cod, errorMessage)
                }
            } else {
                if let error = error {
                    onError(code, error.localizedDescription)
                }
            }
            
        })
    }
    
    
    //    Call 5 day / 3 hour forecast data By geographic coordinates
    //    api.openweathermap.org/data/2.5/forecast?lat=35&lon=139


    /*
        {
          "cod": "200",
          "message": 0.0045,
          "cnt": 40,
          "list": [
            {
              "dt": 1504137600,
              "main": {
                "temp": 285.98,       // температура среднесуточная
                "temp_min": 283.197,  // температура минимальная
                "temp_max": 285.98,   // температура максимальная
                "pressure": 999.65,   // давление
                "sea_level": 1032.65, // уровень моря
                "grnd_level": 999.65,
                "humidity": 100,      // влажность
                "temp_kf": 2.78
              },
              "weather": [
                {
                  "id": 500,
                  "main": "Rain",
                  "description": "light rain",
                  "icon": "10n"
                }
              ],
              "clouds": {
                "all": 76
              },
              "wind": {
                "speed": 1.16,
                "deg": 92.0044
              },
              "rain": {
                "3h": 0.069999999999999
              },
              "sys": {
                "pod": "n"
              },
              "dt_txt": "2017-08-31 00:00:00"
            },
            
          ],
          "city": {
            "id": 1494346,
            "name": "Posëlok Rabochiy",
            "coord": {
              "lat": 56.85,
              "lon": 60.6
            },
            "country": "RU",
            "population": 2000
          }
        }
    */
    

    
    
    func get5DayPer3HourForecastData(forOne location : Location,
                                     onCompletion: @escaping (Array<WeatherOnHour>) -> Void,
                                     onError: @escaping (Int, String) -> Void) {
        
        guard let coordinate = location.coordinate else {
            onError(-1, "The location coordinate is not defined")
            return
        }
        
        
        let units : String
        if SettingsApp.getTemperatureUnits() == .imperial {
            units = "imperial"
        } else {
            units = "metric"
        }
        
        let parameters = ["lat": coordinate.latitude, "lon": coordinate.longitude, "APPID": openWeatherMapApiKey, "units": units] as [String : Any]
        
        HTTPEngine.httpGetRequest(path: openWeatherMapApiHost + "/data/2.5/forecast", parameters: parameters, onCompletion: { (code, json, jsonArray, error) in
            guard let jsonResponse = json else {
                if let error = error {
                    onError(code, error.localizedDescription)
                }
                return
            }
            
            if let cod = jsonResponse["cod"] as? String {
                if cod == "200" {
                    var weatherHourList = Array<WeatherOnHour>()
                
                     if let list = jsonResponse["list"] as? [[String : Any]] {
                        
                        for item in (list) {
                            guard  let dt = item["dt"] as? Double else {
                                return
                            }

                            let weatherHour = WeatherOnHour(for: location,  at: Date(timeIntervalSince1970: dt))
                            
                            
                            if let mainWeather = item["main"] as? [String : Any] {
                                
                                if let sea_level = mainWeather["sea_level"] as? Double {
                                    weatherHour.seaLevel = sea_level
                                }
                                
                                if let temp = mainWeather["temp"] as? Double {
                                    weatherHour.temp = temp
                                }
                                
                                if let temp_min = mainWeather["temp_min"] as? Double {
                                    weatherHour.tempMin = temp_min
                                }
                                
                                if let temp_max = mainWeather["temp_max"] as? Double {
                                    weatherHour.tempMax = temp_max
                                }
                                
                                if let pressure = mainWeather["pressure"] as? Int {
                                    weatherHour.pressure = pressure
                                }
                                
                                if let humidity = mainWeather["humidity"] as? Int {
                                    weatherHour.humidity = humidity
                                }
                                
                            }
                            
                            if let wind = item["wind"] as? [String : Any] {
                                if let windSpeed = wind["speed"] as? Int {
                                    weatherHour.windSpeed = windSpeed
                                }
                                
                                if let windDeg = wind["deg"] as? Int {
                                    weatherHour.windDeg = windDeg
                                }
                            }

                            if let rain = item["rain"] as? [String : Any] {
                                if let rain3h = rain["3h"] as? Double {
                                    weatherHour.rain = rain3h
                                }
                            }
                            
                            if let clouds = item["clouds"] as? [String : Any] {
                                if let cloudsPercent = clouds["all"] as? Int {
                                    weatherHour.clouds = cloudsPercent
                                }
                                
                            }
                            
                            if let sys = item["sys"] as? [String : Any] {
                                
                                if let pod = sys["pod"] as? String {
                                    if pod == "n" {
                                        weatherHour.partsOfDay = .night
                                    } else {
                                        weatherHour.partsOfDay = .afternoon
                                    }
                                }
                                
                            }
                            
                            if let weathers = item["weather"] as? [[String : Any]] {
                                
                                for weather in (weathers) {
                                    var partsOfDay : PartsOfDay = .afternoon
                                    if let weatherIcon = weather["icon"] as? String {
                                        if let icon = getIcon(byIdentifyer: weatherIcon) {
                                            weatherHour.icon = icon
                                        }
                                        partsOfDay = getPartsOfDay(byIdentifyer: weatherIcon)
                                    }
                                    
                                    if let pod = weatherHour.partsOfDay {
                                        partsOfDay = pod
                                    }
                                    
                                    if let weatherIdentifyer = weather["id"] as? Int {
                                        let openWeatherMapWheaterSubType : OpenWeatherMapWheaterSubType = getOpenWeatherMapWheaterType(byIdentifyer: weatherIdentifyer)
                                        
                                        if let detailDescription = getWeatherDetailDescription(byOpenWeatherMapWheaterSubType: openWeatherMapWheaterSubType) {
                                            weatherHour.detail = detailDescription
                                        } else if let description = weather["description"] as? String {
                                            weatherHour.detail = description
                                        }
                                        
                                        if let icon = getIcon(byWehatherSubType: openWeatherMapWheaterSubType, forPartsOfDay: partsOfDay) {
                                            weatherHour.icon = icon
                                        }
                                        
                                    } else if let description = weather["description"] as? String {
                                        weatherHour.detail = description
                                    }
                                }
                            }
                            
                            weatherHourList.append(weatherHour)
                        }
                        
                     }

                    onCompletion(weatherHourList)
                    
                } else {
                    guard let errorMessage = jsonResponse["message"] as? String else {
                        if let error = error {
                            onError(code, error.localizedDescription)
                        }
                        return
                    }
                    
                    if let intCode = Int(cod) {
                        onError(intCode, errorMessage)
                    } else {
                        onError(-1, errorMessage)
                    }
                }
            } else {
                if let error = error {
                    onError(code, error.localizedDescription)
                }
            }
            
        })
    }

    
}


