//
//  WeatherForecastLocationsCoreDataManager.swift
//  WeatherForecast
//
//  Created by Evgeniy on 03.09.17.
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit
import CoreData

let addLocation    = "addLocation"
let removeLocation = "removeLocation"
let weatherForecastLocationsModelName = "WeatherForecast"

// !!! Добавить синхронизацию с iCloud

class WeatherForecastLocationsCoreDataManager: CoreDataManager {
    static let shared: WeatherForecastLocationsCoreDataManager = {
        let instance = WeatherForecastLocationsCoreDataManager(modelName: weatherForecastLocationsModelName)
        return instance
    }()
    
    private let internalQueue = DispatchQueue(label: "WeatherForecastLocationsCoreDataManagerQueue")
    
    private override init(modelName: String) {
        super.init(modelName: modelName)
    }
    
    
    func remove(location: Location) {
        
        DispatchQueue.main.async {
        
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "LocationsStorage")
            deleteFetch.predicate = NSPredicate(format: "region == %@", location.identifyer!)
            
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            do {
                try WeatherForecastCoreDataManager.shared.mainManagedObjectContext.execute(deleteRequest)
                WeatherForecastCoreDataManager.shared.saveContext(onComplete: {() in
                    NotificationCenter.default.post(name: Notification.Name(removeLocation), object: location.identifyer)
                },
                                                                  onError: nil)
            } catch {
                print ("There was an error!!!")
            }
            
        }
    }
    
    func add(location: Location) {
        
        DispatchQueue.main.async {
        
            let entityDescription = NSEntityDescription.entity(forEntityName: "LocationsStorage", in: WeatherForecastCoreDataManager.shared.mainManagedObjectContext)
            let managedObject = NSManagedObject(entity: entityDescription!, insertInto: WeatherForecastCoreDataManager.shared.mainManagedObjectContext)
            
            if let region = location.identifyer {
                managedObject.setValue(region, forKey: "region")
            }
            
            
            if let name = location.name {
                managedObject.setValue(name, forKey: "name")
            }
            
            if let country = location.country {
                managedObject.setValue(country, forKey: "country")
            }
            
            if let coordinate = location.coordinate {
                managedObject.setValue(coordinate.latitude, forKey: "latitude")
                managedObject.setValue(coordinate.longitude, forKey: "longitude")
            }
            
            self.saveContext(onComplete: {() in
                NotificationCenter.default.post(name: Notification.Name(addLocation), object: location.identifyer)
            },
                             onError: nil)
        }
        
    }
    
    func list(onCompletion: @escaping (Array<Location>) -> Void) {
        
    }

}
