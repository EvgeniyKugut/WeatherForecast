//
//  LocationCellConfigurator.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class LocationCellConfigurator: NSObject {

    static func fill(_ cell: LocationCell, withLocation: Location?) {
        if let location = withLocation {
            cell.City.text = location.name
            if let coordinate = location.coordinate {
                cell.Latitude.text = "lat: \(coordinate.latitude)"
                cell.Longitude.text = "lon: \(coordinate.longitude)"
            } else {
                cell.Latitude.text = "lat: --"
                cell.Longitude.text = "lon: --"
            }
            cell.Country.text = location.country
        } else {
            cell.City.text = ""
            cell.Latitude.text = ""
            cell.Longitude.text = ""
        }
        
    }
    
}
