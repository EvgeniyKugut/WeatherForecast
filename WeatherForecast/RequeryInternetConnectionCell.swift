//
//  RequeryInternetConnectionCell.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class RequeryInternetConnectionCell: UITableViewCell {
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var refreshButton: Button!

    func animate() {
        self.refreshButton.shake()
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.animate()
        }

    }

}
