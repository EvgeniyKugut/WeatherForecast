//
//  WeatherEngine.swift
//  WeatherForecast
//
//  Created by Evgeniy on 31.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation
import CoreData
import CoreLocation


class WeatherEngine: NSObject {

    // Обновление прогноза погоды на день в CoreData
    class func updateWeather(forOne location : Location,
                             onCompletion: @escaping (WeatherOnDay) -> Void,
                             onError: @escaping (Int, String) -> Void) {
        
        let openWeatherMap : OpenWeatherMap = OpenWeatherMap()
        openWeatherMap.getCurrentWeatherData(forOne: location,
                                             onCompletion: {(currentWeather) in
                                                
                                                WeatherForecastCoreDataManager.shared.saveCurrentWeather(currentWeather: currentWeather,
                                                                                                         onCompletion: { () in
                                                                                                            onCompletion(currentWeather)
                                                                                                         },
                                                                                                         onError: {(message) in
                                                                                                            print(message)
                                                                                                         })

                                                
                                            },
                                             onError: {(code, message) in
                                                onError(code, message)
                                            })
        
        
    }
    
    // Обновление прогноза погоды в CoreData
    class func updateWeatherHours(forOne location : Location,
                                 onCompletion: @escaping (Array<WeatherOnHour>) -> Void,
                                 onError: @escaping (Int, String) -> Void) {
        
        let openWeatherMap : OpenWeatherMap = OpenWeatherMap()
        
        openWeatherMap.get5DayPer3HourForecastData(forOne: location,
                                                   onCompletion: {(weatherOnHourList) in

                                                    WeatherForecastCoreDataManager.shared.saveToWeatherOnHourStorage(list: weatherOnHourList,
                                                                                                                     onCompletion: { () in
                                                                                                                        
                                                                                                                        onCompletion(weatherOnHourList)
                                                    },
                                                                                                                     onError: {(message) in
                                                                                                                        onError(-1, message)
                                                    })

                                                   },
                                                   onError: {(code, message) in
                                                    onError(code, message)
                                                   })
        
        
    }
    
    
    class func updateWeatherWithCheckChanges(onHasChanges: @escaping (String) -> Void,
                                             onNoChanges: @escaping () -> Void,
                                             onError: @escaping (String) -> Void) {
        
        // перед тем как вызвать onHasChanges(message) нужно сохранить новую погоду в просмотренные, для того чтобы не получать повторной нотификации
        onNoChanges()
        
    }
    
    
    // Групирует данные погоды по дням. Отдельно возвращает дни, и отдельно отсортированные часы в разрезе дней
    class func group(weatherOnHour weatherOnHours: Array<WeatherOnHour>, onCompletion: @escaping (Array<Date>, Array<Array<WeatherOnHour>>) -> Void) {

        let sortedWeatherOnHours = weatherOnHours.sorted { (weatherOnHour1, weatherOnHour2) -> Bool in
            return weatherOnHour1.date! <= weatherOnHour2.date!
        }

        let groupedData : Dictionary <Date, Array<WeatherOnHour>> = sortedWeatherOnHours.grouped(by: { (weatherOnHour: WeatherOnHour) -> Date in
            return weatherOnHour.date!.dateWithoutTime()
        })

        let groupedSortedData = groupedData.sorted() { $0.0 < $1.0 }

        var days : Array<Date> = []
        var hoursByDay : Array<Array<WeatherOnHour>> = []

        hoursByDay = Array<Array<WeatherOnHour>>(groupedSortedData.map({$0.1}))
        for period in groupedSortedData.map({$0.0}) {
            days.append(period)
        }

        onCompletion(days, hoursByDay)
    }

    class func findTemperatureDiff(forWeatherOnHour weatherOnHours1: Array<WeatherOnHour>, andWeatherOnHours weatherOnHours2: Array<WeatherOnHour>) -> (WeatherOnHour?, WeatherOnHour?) {
        return WeatherEngine.findDiff(forWeatherOnHour: weatherOnHours1, andWeatherOnHours: weatherOnHours2, onCheckDiff: {(weatherOnHour1, weatherOnHour2) -> Bool in
            
            guard let temp1 = weatherOnHour1.temp else {
                return false
            }

            guard let temp2 = weatherOnHour2.temp else {
                return false
            }
            
            return abs(temp1 - temp2) >= 3
        })
    }
    
    class func findDiff(forWeatherOnHour weatherOnHours1: Array<WeatherOnHour>, andWeatherOnHours weatherOnHours2: Array<WeatherOnHour>, onCheckDiff: @escaping (WeatherOnHour, WeatherOnHour) -> Bool) -> (WeatherOnHour?, WeatherOnHour?) {
        
        let limits1 = WeatherEngine.getLimitedDateTimes(forWeatherOnHour: weatherOnHours1)
        let limits2 = WeatherEngine.getLimitedDateTimes(forWeatherOnHour: weatherOnHours2)
        
        let minPeriod = min(limits1.0!, limits2.0!)
        let maxPeriod = max(limits1.1!, limits2.1!)
        
        let preparedWeatherOnHours1 = WeatherEngine.trim(weatherOnHour: WeatherEngine.trim(weatherOnHour: weatherOnHours1, from: minPeriod), to: maxPeriod)
        let preparedWeatherOnHours2 = WeatherEngine.trim(weatherOnHour: WeatherEngine.trim(weatherOnHour: weatherOnHours2, from: minPeriod), to: maxPeriod)
        
        for itemWeatherOnHours in (preparedWeatherOnHours1) {
            let resultWeatherOnHours = WeatherEngine.find(weatherOnHour: preparedWeatherOnHours2, forPeriod: itemWeatherOnHours.date!)
            if let result = resultWeatherOnHours {
                if onCheckDiff(itemWeatherOnHours, result) {
                    return (itemWeatherOnHours, result)
                }
            }
        }
        
        return (nil, nil)
    }
    
    class func find(weatherOnHour weatherOnHours: Array<WeatherOnHour>, forPeriod: Date) -> WeatherOnHour? {
        let sortedWeatherOnHours = weatherOnHours.sorted { (weatherOnHour1, weatherOnHour2) -> Bool in
            return weatherOnHour1.date! <= weatherOnHour2.date!
        }
        
        var prevWeatherOnHour : WeatherOnHour? = nil
        for weatherOnHour in (sortedWeatherOnHours) {
            if let date = weatherOnHour.date {
                if date > forPeriod {
                    if let prev = prevWeatherOnHour {
                        return prev
                    }
                    return weatherOnHour
                } else if date == forPeriod {
                    return weatherOnHour
                } else {
                    prevWeatherOnHour = weatherOnHour
                }
            }
        }
        
        if let prev = prevWeatherOnHour {
            return prev
        } else {
            return nil
        }
        
    }
    
    class func getLimitedDateTimes(forWeatherOnHour weatherOnHours: Array<WeatherOnHour>) -> (Date?, Date?) {
        let sortedWeatherOnHours = weatherOnHours.sorted { (weatherOnHour1, weatherOnHour2) -> Bool in
            return weatherOnHour1.date! <= weatherOnHour2.date!
        }

        if sortedWeatherOnHours.count == 0 {
            return (nil, nil)
        } else {
            return (sortedWeatherOnHours[0].date!, sortedWeatherOnHours[sortedWeatherOnHours.count-1].date!)
        }
    }
    
    class func trim(weatherOnHour weatherOnHours: Array<WeatherOnHour>, from: Date) -> Array<WeatherOnHour> {
        var weatherList = Array<WeatherOnHour>()

        let sortedWeatherOnHours = weatherOnHours.sorted { (weatherOnHour1, weatherOnHour2) -> Bool in
            return weatherOnHour1.date! <= weatherOnHour2.date!
        }
        
        var prevWeatherOnHour : WeatherOnHour? = nil
        for weatherOnHour in (sortedWeatherOnHours) {
            if let date = weatherOnHour.date {
                if date > from {
                    if let prev = prevWeatherOnHour {
                       weatherList.append(prev)
                    }
                    weatherList.append(weatherOnHour)
                } else if date == from {
                    weatherList.append(weatherOnHour)
                } else {
                    prevWeatherOnHour = weatherOnHour
                }
            }
        }
        
        return weatherList
    }

    class func trim(weatherOnHour weatherOnHours: Array<WeatherOnHour>, to: Date) -> Array<WeatherOnHour> {
        var weatherList = Array<WeatherOnHour>()
        
        let sortedWeatherOnHours = weatherOnHours.sorted { (weatherOnHour1, weatherOnHour2) -> Bool in
            return weatherOnHour1.date! <= weatherOnHour2.date!
        }
        
        var prevWeatherOnHour : WeatherOnHour? = nil
        for weatherOnHour in (sortedWeatherOnHours) {
            if let date = weatherOnHour.date {
                if date <= to {
                    weatherList.append(weatherOnHour)
                } else {
                    if let prev = prevWeatherOnHour {
                        if let prevDate = prev.date {
                            if prevDate != to {
                                weatherList.append(weatherOnHour)
                            }
                        }
                    }
                    break
                }
            }
            prevWeatherOnHour = weatherOnHour
        }
        
        
        return weatherList
    }
    
    // Получение прогноза погоды из CoreData по часам в разрезе дней
    class func getWeather(forOne location : Location,
                             onCompletion: @escaping (WeatherOnDay) -> Void,
                             onError: @escaping (String) -> Void) {

        WeatherForecastCoreDataManager.shared.loadCurrentWeather(forOne: location,
                                                                 onCompletion: {(weatherOnDay) in
                                                                    onCompletion(weatherOnDay)
                                                                 },
                                                                 onError: {(message) in
                                                                    onError(message)
                                                                 })
        
//        let openWeatherMap : OpenWeatherMap = OpenWeatherMap()
//        openWeatherMap.getCurrentWeatherData(forOne: location,
//                                             onCompletion: {(currentWeather) in
//                                                
//                                                //!!! Need Save to CoreData
//                                                
//                                                onCompletion(currentWeather)
//        },
//                                             onError: {(code, message) in
//                                                onError(message)
//                                                
//        })
        
        
    }
    
    
    // Получение прогноза погоды из CoreData по часам в разрезе дней
    class func getGroupedWeatherByDays(forOne location : Location,
                    onCompletion: @escaping (Array<Date>, Array<Array<WeatherOnHour>>) -> Void,
                    onError: @escaping (String) -> Void) {

        WeatherForecastCoreDataManager.shared.loadFromWeatherOnHourStorage(forOne: location,
                                                                           onCompletion: {(weatherOnHourList) in

                                                                                WeatherEngine.group(weatherOnHour: weatherOnHourList, onCompletion: { (days, hoursByDay) in
                                                                                    onCompletion(days, hoursByDay)
                                                                                })
        
                                                                           },
                                                                           onError: onError)
        
//        //!!! Change to CoreData
//        let openWeatherMap : OpenWeatherMap = OpenWeatherMap()
//        openWeatherMap.get5DayPer3HourForecastData(forOne: location,
//                                                   onCompletion: {(weatherOnHours) in
//                                                    
//                                                        WeatherEngine.group(weatherOnHour: weatherOnHours, onCompletion: { (days, hoursByDay) in
//                                                            onCompletion(days, hoursByDay)
//                                                        })
//                                                    
//                                                   },
//                                                   onError: {(code, message) in
//                                                        onError(message)
//                                                   })
//        

    }
    
}
