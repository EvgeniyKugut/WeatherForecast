//
//  MainWeatherOnDayCellConfigurator.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class MainWeatherOnDayCellConfigurator: NSObject {

    static func fill(_ cell: MainWeatherOnDayCell, withData: WeatherOnDay) {
        if let location = withData.location {
            cell.city.text = location.name
        } else {
            cell.city.text = ""
        }
        
        if let iconName = withData.icon {
            cell.weatherIcon.image = WeatherIcons.shared.getIcon(byName: iconName)
        }
        
        if let temperature = withData.temp  {
            if SettingsApp.getTemperatureUnits() == .metric {
                if temperature > 0 {
                    cell.temperature.text = "+\(Int(temperature))°С"
                } else {
                    cell.temperature.text = "\(Int(temperature))°С"
                }
            } else {
                if temperature > 0 {
                    cell.temperature.text = "+\(Int(temperature))°F"
                } else {
                    cell.temperature.text = "\(Int(temperature))°F"
                }
            }
        } else {
            if SettingsApp.getTemperatureUnits() == .metric {
                cell.temperature.text = "0°С"
            } else {
                cell.temperature.text = "0°F"
            }
        }
        

        if let sunrise = withData.sunrise {
            cell.minTemperature.text = "Восх.  \(sunrise.timeToString(format: "hh:mm"))"
        }
        
        if let sunrise = withData.sunset {
            cell.maxTemperature.text = "Закт.   \(sunrise.timeToString(format: "hh:mm"))"
        }
        
        
        if let weatherDescription = withData.detail {
           cell.weatherDescription.text = weatherDescription.localized
        } else {
           cell.weatherDescription.text = ""
        }
        
    }
    
}
