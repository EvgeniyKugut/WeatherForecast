//
//  Button.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

@IBDesignable
class Button: UIButton {

    @IBInspectable
    public var cornerRadius: CGFloat = 2.0 {
        didSet {
            self.layer.cornerRadius = self.cornerRadius
        }
    }
    
    @IBInspectable var borderColor: UIColor? {
        didSet {
            if let bColor = borderColor {
                self.layer.borderColor = bColor.cgColor
            }
        }
    }
    
    @IBInspectable var borderWidth: CGFloat = 0 {
        didSet {
            self.layer.borderWidth = borderWidth
        }
    }
    
    @IBInspectable var padding: CGFloat = 0
    
    override func prepareForInterfaceBuilder() {
        super.prepareForInterfaceBuilder()
        
    }
    
    override func layoutSubviews() {
        super.layoutSubviews()
        
    }
    
    
    func shake() {
        let animationKey = "position"
        let animation = CABasicAnimation(keyPath: animationKey)
        animation.fromValue = NSValue(cgPoint: CGPoint(x: center.x + 8, y: center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: center.x - 8, y: center.y))
        animation.autoreverses = true
        animation.repeatCount = 5
        animation.duration = 0.08
        layer.add(animation, forKey: animationKey)
    }

}
