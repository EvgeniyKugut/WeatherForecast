//
//  WeatherPageController.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit
import CoreLocation

class WeatherPageController: UIPageViewController, UIPageViewControllerDelegate, UIPageViewControllerDataSource {

    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    lazy var weatherPages : [WeatherPageViewController] = {
        return [self.viewControllerInstance(withLocationType: .currentLocation)]
    } ()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.dataSource = self
        self.delegate   = self
        self.view.backgroundColor = darkBackgroundColor
        
        // Москва
        self.addForceLocation(withLatitude: 55.764156, longitude: 37.609125)
        
        // Санкт-Петербург
        self.addForceLocation(withLatitude: 59.960469, longitude: 30.293877)

        
        if let firstVC = weatherPages.first {
           setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
        }
        
        
    }
    

    func addForceLocation(withLatitude latitude : Double, longitude : Double) {
        let coord = CLLocationCoordinate2D(latitude: latitude, longitude: longitude)
        Location.make(withLocation: coord,
                      onCompletion: {(currentLocation)  in
                        DispatchQueue.main.async {
                            let viewController = self.viewControllerInstance(withLocationType: .prescribedLocation)
                            viewController.location = currentLocation
                            self.weatherPages.append(viewController)
                            self.reloadInputViews()
                            if let firstVC = self.weatherPages.first {
                                self.setViewControllers([firstVC], direction: .forward, animated: true, completion: nil)
                            }
                        }
                        
        },
                      onError: { (message) in
        })

    }
    
    
//    func reloadPages () {
//     //   weatherPages.eq
//    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        for view in self.view.subviews {
            if view is UIScrollView {
                var navigationBarHeight : CGFloat = 0
                if let navigationController = self.navigationController {
                    navigationBarHeight = navigationController.navigationBar.frame.origin.y + navigationController.navigationBar.frame.size.height
                }
                view.frame = CGRect(x: UIScreen.main.bounds.origin.x, y: UIScreen.main.bounds.origin.y, width: UIScreen.main.bounds.width, height: UIScreen.main.bounds.height - navigationBarHeight)
                
            } else if view is UIPageControl {
                view.backgroundColor = UIColor.clear
            }
            
        }
    }
    
    private func viewControllerInstance(withLocationType locationType: WeatherPageLocationType) -> WeatherPageViewController {
        let contoller = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "WeatherPageViewController") as! WeatherPageViewController
        contoller.locationType = locationType
        return contoller
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerBefore viewController: UIViewController) -> UIViewController? {
        guard let weatherPageViewController = viewController as? WeatherPageViewController else {
            return nil
        }
        
        guard let viewControllerIndex = weatherPages.index(of: weatherPageViewController) else {
            return nil
        }
        
        let previousIndex = viewControllerIndex - 1
        
        guard previousIndex >= 0 else {
            return nil
        }
        
        guard weatherPages.count > previousIndex else {
            return nil
        }
        

        return weatherPages[previousIndex]
    }
    
    func pageViewController(_ pageViewController: UIPageViewController, viewControllerAfter viewController: UIViewController) -> UIViewController? {
        guard let weatherPageViewController = viewController as? WeatherPageViewController else {
            return nil
        }
        
        guard let viewControllerIndex = weatherPages.index(of: weatherPageViewController) else {
            return nil
        }
        
        let nextIndex = viewControllerIndex + 1
        
        guard nextIndex < weatherPages.count else {
            return nil
        }
        
        guard weatherPages.count > nextIndex else {
            return nil
        }
        
        
        return weatherPages[nextIndex]
    }
    
    
    func presentationCount(for pageViewController: UIPageViewController) -> Int {
        return weatherPages.count
    }
    
    func presentationIndex(for pageViewController: UIPageViewController) -> Int {
        guard let firstViewContoller = viewControllers?.first,
            let firstViewContollerIndex = weatherPages.index(of: firstViewContoller as! WeatherPageViewController) else {
            return 0
        }
        
        return firstViewContollerIndex
    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
