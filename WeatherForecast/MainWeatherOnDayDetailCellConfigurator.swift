//
//  MainWeatherOnDayDetailCellConfigurator.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit



class MainWeatherOnDayDetailCellConfigurator: NSObject {

    static func fill(_ cell: MainWeatherOnDayDetailCell, withData: (String, String)) {
        cell.title.text = withData.0
        cell.value.text = withData.1
    }
    
    static func makePropertyList(withData: WeatherOnDay) -> [(String, String)] {
        var propertyList = [(String, String)]()

        if let rain = withData.rain {
            propertyList.append(("уровень осадков", "\(rain) мм"))
        } else {
            propertyList.append(("уровень осадков", "\(0.0) мм"))
        }
    
        if let pressure = withData.pressure {
            propertyList.append(("давление", "\(pressure) мм рт. cn."))
        }

        if let humidity = withData.humidity   {
            propertyList.append(("влажность воздуха", "\(humidity) %"))
        }
        
        if let visibility = withData.visibility {
            propertyList.append(("видимость", "\(visibility) м"))
        }
        
        if let windSpeed = withData.windSpeed {
            propertyList.append(("скорость ветра", "\(windSpeed) м/с"))
        }
        
        if let clouds = withData.clouds {
            propertyList.append(("облачность", "\(clouds) %"))
        }
        
        return propertyList
    }
    
}
