//
//  CoreDataManager.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation
import CoreData

//let modelName : String = "WeatherForecast"
//let storageName : String = "WeatherForecast.sqlite"

class CoreDataManager : NSObject {
    private var modelName: String
    
    init(modelName: String) {
        self.modelName = modelName
        super.init()
    }
    
    private lazy var managedObjectModel: NSManagedObjectModel? = {
        guard let modelURL = Bundle.main.url(forResource: self.modelName, withExtension: "momd") else {
            return nil
        }
        
        let managedObjectModel = NSManagedObjectModel(contentsOf: modelURL)
        return managedObjectModel
    } ()
    
    private lazy var persistentStoreURL : URL = {
        let fileManager = FileManager.default
        let documentsDirectoryURL = fileManager.urls(for: .documentDirectory, in: .userDomainMask)[0]
        
        let storeName = "\(self.modelName).sqlite"
        return documentsDirectoryURL.appendingPathComponent(storeName)
    } ()
    
    private lazy var persistentCoordinator: NSPersistentStoreCoordinator? = {
        guard let managedObjectModel = self.managedObjectModel else {
            return nil
        }
        
        let persistentStoreURL = self.persistentStoreURL
        
        let persistentCoordinator = NSPersistentStoreCoordinator(managedObjectModel: managedObjectModel)
        
        do {
            let options = [NSMigratePersistentStoresAutomaticallyOption: true, NSInferMappingModelAutomaticallyOption: true]
            try persistentCoordinator.addPersistentStore(ofType: NSSQLiteStoreType, configurationName: nil, at: persistentStoreURL, options: options)
            
        } catch {
            let error = error as NSError
            print("\(error.localizedDescription)")
        }
        
        return persistentCoordinator
    } ()
    
    private lazy var privateManagedObjectContext : NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .privateQueueConcurrencyType)
        managedObjectContext.persistentStoreCoordinator = self.persistentCoordinator
        return managedObjectContext
    } ()
    
    private(set) lazy var mainManagedObjectContext : NSManagedObjectContext = {
        let managedObjectContext = NSManagedObjectContext(concurrencyType: .mainQueueConcurrencyType)
        managedObjectContext.parent = self.privateManagedObjectContext
        return managedObjectContext
    } ()
    
    func saveContext(onComplete: @escaping () -> Void, onError: ((String) -> Void)?) {

        if (!privateManagedObjectContext.hasChanges && !mainManagedObjectContext.hasChanges) {
            return
        }
        
        mainManagedObjectContext.performAndWait { () -> Void in
            
            do {
                try self.mainManagedObjectContext.save()
            } catch let error as NSError {
                onError?(error.localizedDescription)
            } catch {
                onError?("Undefined error")
            }
            
            self.privateManagedObjectContext.perform({ () -> Void in
                do {
                    try self.privateManagedObjectContext.save()
                    onComplete()
                } catch let error as NSError {
                    onError?(error.localizedDescription)
                } catch {
                    onError?("Undefined error")
                }
            })
        }
    }
}
