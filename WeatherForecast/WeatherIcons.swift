//
//  WeatherIcons.swift
//  WeatherForecast
//
//  Created by Evgeniy on 31.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class WeatherIcons: NSObject {

    
    private var queue = DispatchQueue(label: "EvgeniyKugut.WeatherForecast.WeatherIcons")
        
    private var iconDictionary = Dictionary<String, UIImage>()
    
    static let shared: WeatherIcons = {
        let instance = WeatherIcons()
        return instance
    }()
    
    private override init() {
        super.init()
    }
    
    func getIcon(byName iconName: String) -> UIImage? {
        var icon : UIImage? = nil
        queue.sync {
            if let iconImage = self.iconDictionary[iconName] {
                icon = iconImage
            } else if let image = UIImage(named: iconName) {
                self.iconDictionary[iconName] = image
                icon = image
            }
        }
        return icon
    }
    
}
