//
//  Location.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit


class Location: NSObject {
    
    public private(set) var identifyer : String? = nil
    public private(set) var name       : String? = nil
    public private(set) var country    : String? = nil
    public private(set) var coordinate : CLLocationCoordinate2D? = nil
    
    private override init() {
        super.init()
    }
    
    init(withRegionIdentifyer regionIdentifyer : String, Location locationCoord: CLLocationCoordinate2D, andLocationName locationName: String, andCountry: String) {
        super.init()
        self.identifyer = regionIdentifyer
        self.name       = locationName
        self.coordinate = locationCoord
        self.country    = andCountry
    }
    
    static func make(withLocation locationCoord: CLLocationCoordinate2D,
                     onCompletion: @escaping (Location) -> Void,
                     onError: @escaping (String) -> Void) {
        let geoCoder = CLGeocoder()
        
        let location = CLLocation(latitude: locationCoord.latitude, longitude: locationCoord.longitude)
        geoCoder.reverseGeocodeLocation(location) { placemarks, error in
            
            if let error = error {
                onError(error.localizedDescription)
                return
            } else {
                
                var placeMark: CLPlacemark!
                placeMark = placemarks?[0]
                
                guard let regionIdentifiyer = placeMark.region?.identifier else {
                    onError("Not defined region")
                    return
                }
                
                guard let address = placeMark.addressDictionary else {
                    onError("Not defined addres")
                    return
                }
                
                
                var country : String
                if let currCountry = address["Country"] as? String {
                   country = currCountry
                } else {
                   country = ""
                }
                
                
                
                var locationName : String = ""
                if let city = address["City"] as? String {
                    locationName = city
                }  else if let arrea = address["AdministrativeArea"] as? String {
                    locationName = arrea
                }  else if let country = address["Country"] as? String {
                    locationName = country
                }  else if let inlandWater = address["InlandWater"] as? String {
                    locationName = inlandWater
                }  else if let ocean = address["Ocean"] as? String {
                    locationName = ocean
                }  else if let areasOfInterest = address["AreasOfInterest"] as? String {
                    locationName = areasOfInterest
                }  else {
                    onError("Not defined city")
                    return
                }

                let location = Location(withRegionIdentifyer: regionIdentifiyer, Location: location.coordinate, andLocationName: locationName, andCountry: country)
                onCompletion(location)
            }
            
            
        }
    
    }
}
