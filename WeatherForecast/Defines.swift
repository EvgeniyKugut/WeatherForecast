//
//  Defines.swift
//  WeatherForecast
//
//  Created by Evgeniy on 02.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

let maxColorValue : Float = 255

let lightBackgroundColor : UIColor = UIColor(colorLiteralRed: Float(46) / maxColorValue, green: Float(65) / maxColorValue, blue: Float(85) / maxColorValue, alpha: 1)
let activeBackgroundColor  : UIColor = UIColor(colorLiteralRed: Float(33) / maxColorValue, green: Float(52) / maxColorValue, blue: Float(74) / maxColorValue, alpha: 1)
let darkBackgroundColor  : UIColor = UIColor(colorLiteralRed: Float(16) / maxColorValue, green: Float(31) / maxColorValue, blue: Float(48) / maxColorValue, alpha: 1)
