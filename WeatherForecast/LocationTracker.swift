//
//  LocationTracker.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation
import CoreLocation
import MapKit


let changeLocation : String = "changeLocation"

class LocationTracker : NSObject, CLLocationManagerDelegate {
    
    var locationManager : CLLocationManager!
    
    private var lastLocationCoord : CLLocation? = nil
    public private(set) var location : Location?

    
    static let shared: LocationTracker = {
        let instance = LocationTracker()
        return instance
    }()
    
    override init() {
        super.init()
        
        self.locationManager = CLLocationManager()
        self.locationManager.delegate = self
        self.locationManager.desiredAccuracy = kCLLocationAccuracyThreeKilometers
        self.locationManager.pausesLocationUpdatesAutomatically = true
        
    }
    
    static func start() {
        LocationTracker.shared.startUpdates()
    }
    
    static func stop() {
        LocationTracker.shared.stopUpdates()
    }
    
    func stopUpdates() {
        self.locationManager.stopUpdatingLocation()
    }
    
    func startUpdates() {
        
        self.locationManager.requestAlwaysAuthorization()
        self.updateLastLocation()
        self.locationManager.startUpdatingLocation()
        
    }
    
    static func updateRequest() {
        LocationTracker.shared.locationManager.requestAlwaysAuthorization()
    }
    
    private func updateLastLocation() {
        guard let location = self.lastLocationCoord else {
            self.location = nil
            return
        }
        
        Location.make(withLocation: location.coordinate,
                      onCompletion: {(makedLocation)  in
                        
                        if let currentLocation = self.location {
                            if currentLocation.identifyer != makedLocation.identifyer {
                                NotificationCenter.default.post(name: Notification.Name(changeLocation), object: makedLocation)
                            }
                        } else {
                            NotificationCenter.default.post(name: Notification.Name(changeLocation), object: makedLocation)
                        }
                        
                        self.location = makedLocation
        },
                      onError: { (message) in
                        self.location = nil
        })
        
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        
        self.lastLocationCoord = manager.location
        self.updateLastLocation()
    }
    
}
