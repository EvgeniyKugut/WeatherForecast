//
//  WeatherLocationCellConfigurator.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class WeatherLocationCellConfigurator: NSObject {

    static func fill(_ cell: WeatherLocationCell, withData: WeatherOnDay) {
        if let location = withData.location {
            cell.City.text = location.name
            if let coordinate = location.coordinate {
                cell.Latitude.text = "lat: \(coordinate.latitude)"
                cell.Longitude.text = "lon: \(coordinate.longitude)"
            } else {
                cell.Latitude.text = "lat: --"
                cell.Longitude.text = "lon: --"
            }
        } else {
            cell.City.text = ""
            cell.Latitude.text = ""
            cell.Longitude.text = ""
        }
        
        if let iconName = withData.icon {
            cell.WeatherIcon.image = WeatherIcons.shared.getIcon(byName: iconName)
        }
        
        if let temperature = withData.temp {
            if SettingsApp.getTemperatureUnits() == .metric {
                if temperature > 0 {
                    cell.Temperature.text = "+\(temperature)°С"
                } else {
                    cell.Temperature.text = "\(temperature)°С"
                }
            } else {
                if temperature > 0 {
                    cell.Temperature.text = "+\(temperature)°F"
                } else {
                    cell.Temperature.text = "\(temperature)°F"
                }
            }
        } else {
            if SettingsApp.getTemperatureUnits() == .metric {
                cell.Temperature.text = "0°С"
            } else {
                cell.Temperature.text = "0°F"
            }
        }
        
       
    }
}
