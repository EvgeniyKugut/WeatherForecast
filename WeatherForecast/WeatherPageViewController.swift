//
//  WeatherPageViewController.swift
//  WeatherForecast
//
//  Created by Evgeniy on 31.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit
import CoreLocation

enum WeatherPageLocationType {
    case currentLocation
    case prescribedLocation
    case undefined
}


class WeatherPageViewController: UITableViewController {

    public  var locationType : WeatherPageLocationType = .undefined
    private var weatherOnDay : WeatherOnDay? = nil
    private var dateArray : Array<Date>? = nil
    private var weatherOnHourArrayList : Array<Array<WeatherOnHour>>? = nil
    private var propertyList : [(String, String)]? = nil
    var location: Location? = nil
    private var isShowed : Bool = false
    var requestCount : Int = 0
    var lastUpdatedTime : Date? = nil

    private let internalQueue = DispatchQueue(label: "WeatherPageViewControllerReloadDataQueue")
    private let semaphore = DispatchSemaphore(value: 1)
    
    override var preferredStatusBarStyle: UIStatusBarStyle {
        return .lightContent
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.tableView.rowHeight = UITableViewAutomaticDimension
        self.tableView.estimatedRowHeight = 40
        self.tableView.tableFooterView = UIView(frame: .zero)
        if let refreshControl = self.refreshControl {
            refreshControl.addTarget(self, action: #selector(didReloadData), for: .valueChanged)
        }
        
        if self.locationType == .currentLocation {
            NotificationCenter.default.addObserver(self, selector: #selector(self.setLocationData(notification:)), name: NSNotification.Name(rawValue: changeLocation), object: nil)
            NotificationCenter.default.addObserver(self, selector: #selector(self.setWeatherOnHoursData(notification:)), name: NSNotification.Name(rawValue: getWeatherOnHoursData), object: nil)
            
            if let currentLocation = LocationTracker.shared.location {
                self.location = currentLocation
            } else {
                LocationTracker.start()
            }
        }
        
        self.didReloadData(false) // сначала отобразим только из CoreData
        self.didReloadData() // Затем запросим обновление CoreData и отобразим
    }
    
    deinit {
        NotificationCenter.default.removeObserver(self)
    }
    
    @IBAction func refreshButtonTap(_ sender: Button) {
        if self.locationType == .currentLocation {
            if let currentLocation = LocationTracker.shared.location {
                self.location = currentLocation
            } else {
                LocationTracker.start()
            }
        }
        self.didReloadData()
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.isShowed = true
    }
    
    override func viewDidDisappear(_ animated: Bool) {
        self.tableView.setContentOffset(CGPoint.zero, animated: false)
        self.isShowed = false
        DispatchQueue.main.async {
            if let refreshControl = self.refreshControl {
                refreshControl.endRefreshing()
            }
        }

        super.viewDidDisappear(animated)
    }
    
    func setLocationData(notification: NSNotification) {
        DispatchQueue.main.async {
            if let currentLocation = notification.object as? Location {
                self.location = currentLocation
                self.didReloadData()
            }
        }
    }
    
    
    func setWeatherOnHoursData(notification: NSNotification) {
        DispatchQueue.main.async {
            if let currentLocation = self.location {
                if let locationRegion = notification.object as? String {
                    if let currentLocationRegion = currentLocation.identifyer {
                        if locationRegion == currentLocationRegion {
                            if var lastUpdatedTime = self.lastUpdatedTime {
                                lastUpdatedTime.addTimeInterval(3)
                                if (Date() > lastUpdatedTime) {
                                    self.didReloadData(false)
                                }
                            } else {
                                self.didReloadData(false)
                            }
                        }
                    }
                }
            }
        }
    }
    
    
    func updateTable() {
        DispatchQueue.main.async {
            if let refreshControl = self.refreshControl {
                refreshControl.endRefreshing()
            }
            self.tableView.reloadData()
        }
    }
    
    
    func didReloadData(_ reload: Bool = true) {
        
        internalQueue.async {
            self.semaphore.wait()
        
            if let currentLocation = self.location {
                if let refreshControl = self.refreshControl, self.isShowed, reload {
                    refreshControl.beginRefreshing()
                }
                
                if reload {
                    WeatherEngine.updateWeather(forOne: currentLocation, onCompletion: {(lastWeatherOnDay) in
                            self.weatherOnDay = lastWeatherOnDay
                            self.propertyList = MainWeatherOnDayDetailCellConfigurator.makePropertyList(withData: lastWeatherOnDay)

                            WeatherEngine.updateWeatherHours(forOne: currentLocation, onCompletion: {(weatherOnHourList) in
                                                                                        WeatherEngine.group(weatherOnHour: weatherOnHourList, onCompletion: { (lastDateArray, lastWeatherOnHourArrayList) in
                                                                                            self.dateArray = lastDateArray
                                                                                            self.weatherOnHourArrayList = lastWeatherOnHourArrayList
                                    
                                                                                            self.requestCount+=1
                                                                                            self.updateTable()
                                                                                            self.semaphore.signal()
                                                                                            self.lastUpdatedTime = Date()
                                                                                        })
                                                            },
                                                             onError: {(Int, String) in
                                                                self.requestCount+=1
                                                                self.updateTable()
                                                                self.semaphore.signal()
                                                            })

                    }, onError: {(Int, Message) in
                        self.requestCount+=1
                        self.updateTable()
                        self.semaphore.signal()
                    })
                } else {
                    WeatherEngine.getWeather(forOne: currentLocation, onCompletion: { (lastWeatherOnDay) in
                        self.weatherOnDay = lastWeatherOnDay
                        self.propertyList = MainWeatherOnDayDetailCellConfigurator.makePropertyList(withData: lastWeatherOnDay)
                        
                        
                        WeatherEngine.getGroupedWeatherByDays(forOne: currentLocation, onCompletion: { (lastDateArray, lastWeatherOnHourArrayList) in
                            self.dateArray = lastDateArray
                            self.weatherOnHourArrayList = lastWeatherOnHourArrayList
                            
                            self.requestCount+=1
                            self.updateTable()
                            self.semaphore.signal()
                            self.lastUpdatedTime = Date()
                        }, onError: { (message) in
                            self.requestCount+=1
                            self.updateTable()
                            self.semaphore.signal()
                        })
                        
                    }, onError: { (message) in
                        self.requestCount+=1
                        self.updateTable()
                        self.semaphore.signal()
                        
                    })
                }

                
            } else {
                self.updateTable()
                self.semaphore.signal()
            }
        }
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }

    // MARK: - Table view data source
    override func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return UITableViewAutomaticDimension
    }
    

    override func numberOfSections(in tableView: UITableView) -> Int {
        
        var hasWeatherData : Bool = false

        if let _ = self.propertyList {
            hasWeatherData = true
        }
        
        if let _ = self.weatherOnDay {
            hasWeatherData = true
        }
        
        if let _ = self.weatherOnHourArrayList {
            hasWeatherData = true
        }
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            return 1
        } else if let _ = self.weatherOnDay {
            return 1
        } else if hasWeatherData {
          return 1
        } else {
            return 1
        }
        
        
    }

    override func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        var records : Int = 0
        if let propertyList = self.propertyList {
            records += propertyList.count
        }
        
        if let _ = self.weatherOnDay {
            records += 1
        }
        
        if let weatherOnHourArrayList = self.weatherOnHourArrayList {
            records += weatherOnHourArrayList.count
        }
        
        
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            return 1
        } else if let _ = self.weatherOnDay {

            return records
        } else {
            return 1
        }
    }

    override func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {

        var propertyListCount : Int = 0

        var hasWeatherData : Bool = false
        if let propertyList = self.propertyList {
            propertyListCount = propertyList.count
            hasWeatherData = true
        }
        
        if let _ = self.weatherOnDay {
            hasWeatherData = true
        }
        
        if let _ = self.weatherOnHourArrayList {
            hasWeatherData = true
        }
    
        if CLLocationManager.authorizationStatus() != .authorizedAlways {
            let cell = tableView.dequeueReusableCell(withIdentifier: "RequeryAllowLocationCell", for: indexPath) as! RequeryAllowLocationCell

            return cell
        } else if indexPath.row == 0 && hasWeatherData {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainWeatherOnDayCell", for: indexPath) as! MainWeatherOnDayCell
            
            if let weatherOnDay = self.weatherOnDay {
                MainWeatherOnDayCellConfigurator.fill(cell, withData: weatherOnDay)
            }
            
            return cell
        } else if indexPath.row <= propertyListCount  && hasWeatherData {
            let cell = tableView.dequeueReusableCell(withIdentifier: "MainWeatherOnDayDetailCell", for: indexPath) as! MainWeatherOnDayDetailCell
            if let propertyList = self.propertyList {
                let index = indexPath.row - 1
                MainWeatherOnDayDetailCellConfigurator.fill(cell, withData: propertyList[index])
            }
            return cell
        } else if hasWeatherData {
            
            let cell = tableView.dequeueReusableCell(withIdentifier: "WeatherOnHoursCell", for: indexPath) as! WeatherOnHoursCell
            if let dateArray = self.dateArray {
                if let weatherOnHourArrayList = self.weatherOnHourArrayList {
                    let index = indexPath.row - 1 - propertyListCount
                    if index < dateArray.count && index < weatherOnHourArrayList.count {
                        cell.fill(withDate: dateArray[index], andData: weatherOnHourArrayList[index], isEven: index % 2 == 1)
                    }
                }
            }

            return cell
        } else {
            if !Reachability.isInternetAvailable() {
                let cell = tableView.dequeueReusableCell(withIdentifier: "RequeryInternetConnectionCell", for: indexPath) as! RequeryInternetConnectionCell
                return cell
            } else {
                let cell = tableView.dequeueReusableCell(withIdentifier: "WaitFinishWeatherRequeryCell", for: indexPath) as! WaitFinishWeatherRequeryCell
                return cell
            }
            
        }

    }

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}
