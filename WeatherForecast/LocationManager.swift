//
//  LocationManager.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation

class LocationManager: NSObject {

    func add(location: Location) {
        WeatherForecastLocationsCoreDataManager.shared.add(location: location)
    }
    
    func remove(location: Location) {
        WeatherForecastLocationsCoreDataManager.shared.remove(location: location)
    }
    
    func list(onCompletion: @escaping (Array<Location>) -> Void) {
        WeatherForecastLocationsCoreDataManager.shared.list(onCompletion: onCompletion)
    }
    
}
