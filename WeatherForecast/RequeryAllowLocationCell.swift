//
//  RequeryAllowLocationCell.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class RequeryAllowLocationCell: UITableViewCell {
    @IBOutlet weak var message: UILabel!
    @IBOutlet weak var settingsButton: Button!
    
    @IBAction func settingsButtonTap(_ sender: Button) {
        SettingsApp.open()
    }
    
    func animate() {
        self.settingsButton.shake()
    }

    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none
        

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        if selected {
            self.animate()
        }

        // Configure the view for the selected state
    }

}
