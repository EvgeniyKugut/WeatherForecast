//
//  LocalPushNotificator.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit
import UserNotifications

class LocalPushNotificator: NSObject {
    
    override init() {
        super.init()
    }
    
    static func pushRequest() {
        if #available(iOS 10.0, *) {
            let center = UNUserNotificationCenter.current()
            center.requestAuthorization(options: [.alert, .sound]) { (granted, error) in
                // Enable or disable features based on authorization.
                
            }
        } else {
            let notificationSettings = UIUserNotificationSettings(types: [.alert, .badge, .sound], categories: nil)
            UIApplication.shared.registerUserNotificationSettings(notificationSettings)
            
        }
        
    }
    
    static func push(message: String) {
        
        let notification = UILocalNotification()
        notification.alertBody = message
        notification.alertAction = "open"
        let uuid = NSUUID().uuidString
        notification.userInfo = ["UUID": uuid]
        UIApplication.shared.scheduleLocalNotification(notification)
    }
    
}
