//
//  WeatherForecastCoreDataManager.swift
//  WeatherForecast
//
//  Created by Evgeniy on 03.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit
import CoreData

let getCurrentWeatherData    = "getCurrentWeatherData"
let getWeatherOnHoursData    = "getWeatherOnHoursData"
let weatherForecastModelName = "WeatherForecast"
let viewedWeatherOnHourStorage = "ViewedWeatherOnHourStorage"
let weatherOnHourStorage = "WeatherOnHourStorage"

class WeatherForecastCoreDataManager: CoreDataManager {
    static let shared: WeatherForecastCoreDataManager = {
        let instance = WeatherForecastCoreDataManager(modelName: weatherForecastModelName)
        return instance
    }()
    
    private let internalQueue = DispatchQueue(label: "WeatherForecastCoreDataManagerQueue")

    private override init(modelName: String) {
        super.init(modelName: modelName)
    }
    

    func loadCurrentWeather(forOne location : Location,
                            onCompletion: @escaping (WeatherOnDay) -> Void,
                            onError: @escaping (String) -> Void) {
        
        DispatchQueue.main.async {
        
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherOnDayStorage")
            fetchRequest.predicate = NSPredicate(format: "region == %@", location.identifyer!)
            
            do {
                let results : Array<NSFetchRequestResult> = try self.mainManagedObjectContext.fetch(fetchRequest)
                if results.count == 1 {
                    if let weatherOnDayData = results[0] as? NSManagedObject {
                        guard let date = weatherOnDayData.value(forKey: "date") as? Date else {
                            self.internalQueue.async {
                                onError("Data is incorrect")
                            }
                            return
                        }
                        
                        let weatherOnDay : WeatherOnDay = WeatherOnDay(for: location, at: date)
                        
                        if let sunset     = weatherOnDayData.value(forKey: "sunset") as? Date {   // время заката
                            weatherOnDay.sunset = sunset
                        }
                        
                        if let sunrise    = weatherOnDayData.value(forKey: "sunrise") as? Date {   // время восхода
                            weatherOnDay.sunrise = sunrise
                        }
                        
                        if let temp       = weatherOnDayData.value(forKey: "temp") as? Double { // текущая температура
                            weatherOnDay.temp = temp
                        }
                        
                        if let tempMin    = weatherOnDayData.value(forKey: "temp_min") as? Double { // минимальная температура за сутки
                            weatherOnDay.tempMin = tempMin
                        }
                        
                        if let tempMax    = weatherOnDayData.value(forKey: "temp_max") as? Double { // максимальная температура за сутки
                            weatherOnDay.tempMax = tempMax
                        }
                        
                        if let pressure   = weatherOnDayData.value(forKey: "pressure") as? Int {    // атмосферное давление
                            weatherOnDay.pressure = pressure
                        }
                        
                        if let humidity   = weatherOnDayData.value(forKey: "humidity") as? Int {    // влажность воздуха
                            weatherOnDay.humidity = humidity
                        }
                        
                        if let visibility = weatherOnDayData.value(forKey: "visibility") as? Int {    // видимость
                            weatherOnDay.visibility = visibility
                        }
                        
                        if let windSpeed  = weatherOnDayData.value(forKey: "wind_speed") as? Int {    // скорость ветра
                            weatherOnDay.windSpeed = windSpeed
                        }
                        
                        if let windDeg    = weatherOnDayData.value(forKey: "wind_deg") as? Int {    // направление ветра
                            weatherOnDay.windDeg = windDeg
                        }
                        
                        if let clouds     = weatherOnDayData.value(forKey: "clouds") as? Int {    // облачность
                            weatherOnDay.clouds = clouds
                        }
                        
                        if let rain       = weatherOnDayData.value(forKey: "rain") as? Double { // Уровень осадков
                            weatherOnDay.rain = rain
                        }
                        
                        if let icon       = weatherOnDayData.value(forKey: "icon") as? String { // описание
                            weatherOnDay.icon = icon
                        }
                        
                        if let detail     = weatherOnDayData.value(forKey: "detail") as? String { // детальное описание
                            weatherOnDay.detail = detail
                        }
                        
                        self.internalQueue.async {
                            onCompletion(weatherOnDay)
                        }
                    }
                } else {
                    self.internalQueue.async {
                        onError("Data not found")
                    }
                }
                
            } catch {
                self.internalQueue.async {
                    onError(error.localizedDescription)
                }
            }
        }
        
    }
    
    
    func saveCurrentWeather(currentWeather: WeatherOnDay,
                            onCompletion: @escaping () -> Void,
                            onError: @escaping (String) -> Void)
    {
        DispatchQueue.main.async {
        
            guard let location = currentWeather.location else {
                return
            }
            
            // Clear data for region
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: "WeatherOnDayStorage")
            deleteFetch.predicate = NSPredicate(format: "region == %@", location.identifyer!)
            
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            do {
                try WeatherForecastCoreDataManager.shared.mainManagedObjectContext.execute(deleteRequest)
                WeatherForecastCoreDataManager.shared.saveContext(onComplete: {() in
                },
                                                                  onError: nil)
            } catch {
                //!!! Обработать ошибки!!!
                print ("There was an error!!!")
            }
            
            
            // Save to Core Data
            let entityDescription = NSEntityDescription.entity(forEntityName: "WeatherOnDayStorage", in: WeatherForecastCoreDataManager.shared.mainManagedObjectContext)
            let managedObject = NSManagedObject(entity: entityDescription!, insertInto: WeatherForecastCoreDataManager.shared.mainManagedObjectContext)
            
            if let location = currentWeather.location {
                if let region = location.identifyer {
                    managedObject.setValue(region, forKey: "region")
                }
            }
            
            if let date = currentWeather.date {
                managedObject.setValue(date, forKey: "date")
            }
            
            if let sunset = currentWeather.sunset {
                managedObject.setValue(sunset, forKey: "sunset")
            }
            
            if let sunrise = currentWeather.sunrise {
                managedObject.setValue(sunrise, forKey: "sunrise")
            }
            
            if let temp = currentWeather.temp {
                managedObject.setValue(temp, forKey: "temp")
            }
            
            if let tempMin = currentWeather.tempMin {
                managedObject.setValue(tempMin, forKey: "temp_min")
            }
            
            if let tempMax = currentWeather.tempMax {
                managedObject.setValue(tempMax, forKey: "temp_max")
            }
            
            if let pressure = currentWeather.pressure {
                managedObject.setValue(pressure, forKey: "pressure")
            }
            
            
            if let humidity = currentWeather.humidity {
                managedObject.setValue(humidity, forKey: "humidity")
            }
            
            if let visibility = currentWeather.visibility {
                managedObject.setValue(visibility, forKey: "visibility")
            }
            
            
            if let windSpeed = currentWeather.windSpeed {
                managedObject.setValue(windSpeed, forKey: "wind_speed")
            }
            
            if let windDeg = currentWeather.windDeg {
                managedObject.setValue(windDeg, forKey: "wind_deg")
            }
            
            
            if let clouds = currentWeather.clouds {
                managedObject.setValue(clouds, forKey: "clouds")
            }
            
            if let rain = currentWeather.rain {
                managedObject.setValue(rain, forKey: "rain")
            }
            
            if let icon = currentWeather.icon {
                managedObject.setValue(icon, forKey: "icon")
            }
            
            if let detail = currentWeather.detail {
                managedObject.setValue(detail, forKey: "detail")
            }
            
            
            self.saveContext(onComplete: {() in
                self.internalQueue.async {
                    onCompletion()
                }
                NotificationCenter.default.post(name: Notification.Name(getCurrentWeatherData), object: currentWeather.location!.identifyer)
            },
                             onError: {(message) in
                                self.internalQueue.async {
                                    onError(message)
                                }
            })
        }
    }

    func saveToViewedWeatherOnHourStorage(list: Array<WeatherOnHour>,
                                          onCompletion: @escaping () -> Void,
                                          onError: @escaping (String) -> Void) {
        saveWeatherOnHour(list: list, toEntity: viewedWeatherOnHourStorage, onCompletion: onCompletion, onError: onError)
    }

    func saveToWeatherOnHourStorage(list: Array<WeatherOnHour>,
                                    onCompletion: @escaping () -> Void,
                                    onError: @escaping (String) -> Void) {
        saveWeatherOnHour(list: list, toEntity: weatherOnHourStorage, onCompletion: onCompletion, onError: onError)
    }

    
    
    private func saveWeatherOnHour(list: Array<WeatherOnHour>, toEntity: String,
                                   onCompletion: @escaping () -> Void,
                                   onError: @escaping (String) -> Void) {

        DispatchQueue.main.async {
        
            if list.count == 0 {
                return
            }
            
            guard let location = list[0].location else {
                return
            }
            
            
            // Clear data for region
            let deleteFetch = NSFetchRequest<NSFetchRequestResult>(entityName: toEntity)
            deleteFetch.predicate = NSPredicate(format: "region == %@", location.identifyer!)
            
            let deleteRequest = NSBatchDeleteRequest(fetchRequest: deleteFetch)
            
            
            do {
                try WeatherForecastCoreDataManager.shared.mainManagedObjectContext.execute(deleteRequest)
                self.saveContext(onComplete: {() in
                },
                                 onError: nil)
            } catch {
                //!!! Обработать ошибки!!!
                print ("There was an error!!!")
            }
            
            
            var anyLocation : Location? = nil
            
            // Save
            for  weatherHour in (list) {
                let entityDescription = NSEntityDescription.entity(forEntityName: toEntity, in: WeatherForecastCoreDataManager.shared.mainManagedObjectContext)
                let managedObject = NSManagedObject(entity: entityDescription!, insertInto: WeatherForecastCoreDataManager.shared.mainManagedObjectContext)
                
                
                if let location = weatherHour.location {
                    anyLocation = location
                    if let region = location.identifyer {
                        managedObject.setValue(region, forKey: "region")
                    }
                }
                
                if let date = weatherHour.date {
                    managedObject.setValue(date, forKey: "date")
                }
                
                if let temp = weatherHour.temp {
                    managedObject.setValue(temp, forKey: "temp")
                }
                
                if let tempMin = weatherHour.tempMin {
                    managedObject.setValue(tempMin, forKey: "temp_min")
                }
                
                if let tempMax = weatherHour.tempMax {
                    managedObject.setValue(tempMax, forKey: "temp_max")
                }
                
                if let pressure = weatherHour.pressure {
                    managedObject.setValue(pressure, forKey: "pressure")
                }
                
                if let humidity = weatherHour.humidity {
                    managedObject.setValue(humidity, forKey: "humidity")
                }
                
                if let windSpeed = weatherHour.windSpeed {
                    managedObject.setValue(windSpeed, forKey: "wind_speed")
                }
                
                if let windDeg = weatherHour.windDeg {
                    managedObject.setValue(windDeg, forKey: "wind_deg")
                }
                
                if let clouds = weatherHour.clouds {
                    managedObject.setValue(clouds, forKey: "clouds")
                }
                
                if let partsOfDay = weatherHour.partsOfDay {
                    managedObject.setValue(partsOfDay == .afternoon, forKey: "is_day")
                }
                
                if let rain = weatherHour.rain {
                    managedObject.setValue(rain, forKey: "rain")
                }
                
                if let seaLevel = weatherHour.seaLevel {
                    managedObject.setValue(seaLevel, forKey: "sea_level")
                }
                
                if let icon = weatherHour.icon {
                    managedObject.setValue(icon, forKey: "icon")
                }
                
                if let detail = weatherHour.detail {
                    managedObject.setValue(detail, forKey: "detail")
                }
            }
            
            if let anyLocation = anyLocation {
                self.saveContext(onComplete: {() in
                    self.internalQueue.async {
                        onCompletion()
                    }
                    if toEntity == weatherOnHourStorage {
                        NotificationCenter.default.post(name: Notification.Name(getWeatherOnHoursData), object: anyLocation.identifyer)
                    }
                    
                },
                                 onError: {(message) in
                                    self.internalQueue.async {
                                        onError(message)
                                    }
                })
            }
        }
    }

    
    func loadFromViewedWeatherOnHourStorage(forOne location : Location,
                                          onCompletion: @escaping (Array<WeatherOnHour>) -> Void,
                                          onError: @escaping (String) -> Void) {
        loadWeatherOnHour(forOne: location, toEntity: viewedWeatherOnHourStorage, onCompletion: onCompletion, onError: onError)
    }
    
    func loadFromWeatherOnHourStorage(forOne location : Location,
                                    onCompletion: @escaping (Array<WeatherOnHour>) -> Void,
                                    onError: @escaping (String) -> Void) {
        loadWeatherOnHour(forOne: location, toEntity: weatherOnHourStorage, onCompletion: onCompletion, onError: onError)
    }

    
    private func loadWeatherOnHour(forOne location : Location,
                            toEntity: String,
                            onCompletion: @escaping (Array<WeatherOnHour>) -> Void,
                            onError: @escaping (String) -> Void) {
        
        DispatchQueue.main.async {
            let fetchRequest = NSFetchRequest<NSFetchRequestResult>(entityName: toEntity)
            fetchRequest.predicate = NSPredicate(format: "region == %@", location.identifyer!)
            
            do {
                let results : Array<NSFetchRequestResult> = try self.mainManagedObjectContext.fetch(fetchRequest)
                if results.count > 0 {
                    var weatherOnHourList : Array<WeatherOnHour> = []
                    
                    let count : NSInteger = results.count
                    for i in (0 ..< count) {
                        
                        if let weatherOnHourData = results[i] as? NSManagedObject {
                            guard let date = weatherOnHourData.value(forKey: "date") as? Date else {
                                self.internalQueue.async {
                                    onError("Data is incorrect")
                                }
                                return
                            }
                            
                            let weatherOnHour : WeatherOnHour = WeatherOnHour(for: location, at: date)
                            
                            
                            if let temp        = weatherOnHourData.value(forKey: "temp") as? Double {     // текущая температура
                                weatherOnHour.temp = temp
                            }
                            
                            if let tempMin     = weatherOnHourData.value(forKey: "temp_min") as? Double {     // минимальная температура за сутки
                                weatherOnHour.tempMin = tempMin
                            }
                            
                            if let tempMax     = weatherOnHourData.value(forKey: "temp_max") as? Double {     // максимальная температура за сутки
                                weatherOnHour.tempMax = tempMax
                            }
                            
                            if let pressure    = weatherOnHourData.value(forKey: "pressure") as? Int {        // атмосферное давление
                                weatherOnHour.pressure = pressure
                            }
                            
                            if let humidity    = weatherOnHourData.value(forKey: "humidity") as? Int {        // влажность воздуха
                                weatherOnHour.humidity = humidity
                            }
                            
                            if let windSpeed   = weatherOnHourData.value(forKey: "wind_speed") as? Int {        // скорость ветра
                                weatherOnHour.windSpeed = windSpeed
                            }
                            
                            if let windDeg     = weatherOnHourData.value(forKey: "wind_deg") as? Int {        // направление ветра
                                weatherOnHour.windDeg = windDeg
                            }
                            
                            if let clouds      = weatherOnHourData.value(forKey: "clouds") as? Int {        // облачность
                                weatherOnHour.clouds = clouds
                            }
                            
                            if let partsOfDay  = weatherOnHourData.value(forKey: "is_day") as? Bool { // время суток (день/ночь)
                                if partsOfDay {
                                    weatherOnHour.partsOfDay = .afternoon
                                } else {
                                    weatherOnHour.partsOfDay = .night
                                }
                            }
                            
                            if let rain        = weatherOnHourData.value(forKey: "rain") as? Double {     // Уровень осадков
                                weatherOnHour.rain = rain
                            }
                            
                            if let seaLevel    = weatherOnHourData.value(forKey: "sea_level") as? Double {     // Уровень моря
                                weatherOnHour.seaLevel = seaLevel
                            }
                            
                            if let icon        = weatherOnHourData.value(forKey: "icon") as? String {     // описание
                                weatherOnHour.icon = icon
                            }
                            
                            if let detail      = weatherOnHourData.value(forKey: "detail") as? String {     // детальное описание
                                weatherOnHour.detail = detail
                            }
                            
                            
                            weatherOnHourList.append(weatherOnHour)
                        }
                    }
                    
                    self.internalQueue.async {
                        onCompletion(weatherOnHourList)
                    }
                    
                } else {
                    self.internalQueue.async {
                        onError("Data not found")
                    }
                }
                
            } catch {
                self.internalQueue.async {
                    onError(error.localizedDescription)
                }
            }
            
        }
        
    }
    
}
