//
//  SettingsApp.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

let minut : Int = 60

enum TemperatureUnits {
    case metric
    case imperial
}

class SettingsApp: NSObject {

    static let shared: SettingsApp = {
        let instance = SettingsApp()
        return instance
    }()
    
    private override init() {
        super.init()
    }
    
    class func getTemperatureUnits() -> TemperatureUnits {
        return .metric
    }
    
    class func minimumBackgroundFetchInterval() -> TimeInterval {
        return TimeInterval(20 * minut)
    }
    
    class func currentApiService() -> (String, String) {
        return ("OpenWeatherMap", "https://openweathermap.org")
    }
    
    class func open() {
        if let settingsUrl = URL(string: UIApplicationOpenSettingsURLString) {
            if #available(iOS 10.0, *) {
                UIApplication.shared.open(settingsUrl, options: [:], completionHandler: nil)
                
            } else {
                UIApplication.shared.openURL(settingsUrl)
            }
        }
    }
}
