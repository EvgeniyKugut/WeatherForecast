//
//  Extensions.swift
//  WeatherForecast
//
//  Created by Evgeniy on 30.08.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import Foundation

extension String {
    var localized: String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: "")
    }
    
    func localized(withComment:String) -> String {
        return NSLocalizedString(self, tableName: nil, bundle: Bundle.main, value: "", comment: withComment)
    }
    
}

extension Date {
    
    func dayOfWeek() -> String? {
        let dateFormatter = DateFormatter()
        dateFormatter.dateFormat = "EEEE"
        return dateFormatter.string(from: self).capitalized
    }

    func timeToString (format : String = "H:mm") -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = format
        return formatter.string(from: self)
    }
    
    
    func toString () -> String {
        let formatter = DateFormatter()
        formatter.timeZone = TimeZone.current
        formatter.dateFormat = "yyyy/MM/dd hh:mm:ss"
        return formatter.string(from: self)
    }
    
    func dateWithoutTime() -> Date {
        let timeZone = TimeZone.current
        let timeIntervalWithTimeZone = self.timeIntervalSinceReferenceDate + Double(timeZone.secondsFromGMT())
        let timeInterval = floor(timeIntervalWithTimeZone / 86400) * 86400
        return Date(timeIntervalSinceReferenceDate: timeInterval)
    }
    
    
   static func make(withYear year: Int, month : Int, day : Int, hour : Int, minut : Int, second : Int) -> Date? {
    
        var timeInterval = DateComponents()
        timeInterval.year = year
        timeInterval.month = month
        timeInterval.day = day
        timeInterval.hour = hour
        timeInterval.minute = minut
        timeInterval.second = second
        timeInterval.timeZone = TimeZone.current
    
        return Calendar.current.date(from: timeInterval)
    }
    
    func add(withYear year: Int, month : Int, day : Int, hour : Int, minut : Int, second : Int) -> Date? {
        
        var timeInterval = DateComponents()
        timeInterval.year = year
        timeInterval.month = month
        timeInterval.day = day
        timeInterval.hour = hour
        timeInterval.minute = minut
        timeInterval.second = second
        
        return Calendar.current.date(byAdding: timeInterval, to: self)
    }
    
    
}

extension Array {

    func grouped<T>(by criteria: (Element) -> T) -> [T: [Element]] {
        var groups = [T: [Element]]()
        for element in self {
            let key = criteria(element)
            if groups.keys.contains(key) == false {
                groups[key] = [Element]()
            }
            groups[key]?.append(element)
        }
        return groups
    }
}

