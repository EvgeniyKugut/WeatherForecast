//
//  WeatherOnHoursCellConfigurator.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class WeatherOnHoursCellConfigurator: NSObject, UICollectionViewDelegate, UICollectionViewDataSource {

    var dataSource : Array<WeatherOnHour>? = nil
    var cell: WeatherOnHoursCell? = nil
    
    func reload(withCell cell: WeatherOnHoursCell, withDate: Date, andData: Array<WeatherOnHour>) {
        self.cell = cell
        self.dataSource = andData
        cell.dayDescription.text = withDate.dayOfWeek()
        cell.hoursCollection.delegate = self
        cell.hoursCollection.dataSource = self
        cell.hoursCollection.reloadData()
    }
    

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let dataSource = self.dataSource else {
            return 0
        }
        
        return dataSource.count
    }
    
    
 
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "WhetherOnHourCell", for: indexPath) as! WhetherOnHourCell

        guard let weatherOnHour = self.dataSource?[indexPath.row] else {
            return cell
        }
        
        cell.timeDescription.text = weatherOnHour.date?.timeToString()
        cell.weatherIcon.image = WeatherIcons.shared.getIcon(byName: weatherOnHour.icon!)
        
        
        if let temperature = weatherOnHour.temp {
            if SettingsApp.getTemperatureUnits() == .metric {
                if temperature > 0 {
                    cell.temperatureDescription.text = "+\(Int(temperature))°С"
                } else {
                    cell.temperatureDescription.text = "\(Int(temperature))°С"
                }
            } else {
                if temperature > 0 {
                    cell.temperatureDescription.text = "+\(Int(temperature))°F"
                } else {
                    cell.temperatureDescription.text = "\(Int(temperature))°F"
                }
            }
        } else {
            if SettingsApp.getTemperatureUnits() == .metric {
                cell.temperatureDescription.text = "--°С"
            } else {
                cell.temperatureDescription.text = "--°F"
            }
        }
        
        
        
        return cell
    }
    
    
    
}
