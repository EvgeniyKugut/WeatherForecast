//
//  WeatherOnHoursCell.swift
//  WeatherForecast
//
//  Created by Evgeniy on 01.09.17.
//  Email: Evgeniy.Kugut@gmail.com
//  Skype: evgeniy.kugut
//  Copyright © 2017 Evgeniy Kugut. All rights reserved.
//

import UIKit

class WeatherOnHoursCell: UITableViewCell {
    @IBOutlet weak var dayDescription: UILabel!
    @IBOutlet weak var hoursCollection: UICollectionView!
    var weatherOnHoursCellConfigurator : WeatherOnHoursCellConfigurator = WeatherOnHoursCellConfigurator()

    func fill(withDate: Date, andData: Array<WeatherOnHour>, isEven: Bool) {
        weatherOnHoursCellConfigurator.reload(withCell: self, withDate: withDate, andData: andData)
        if !isEven {
            self.backgroundColor                 = lightBackgroundColor
            self.hoursCollection.backgroundColor = lightBackgroundColor
        } else {
            self.backgroundColor                 = activeBackgroundColor
            self.hoursCollection.backgroundColor = activeBackgroundColor
        }
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        self.selectionStyle = .none

        // Initialization code
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
    }

}
